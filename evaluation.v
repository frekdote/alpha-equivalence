From mathcomp Require Import all_ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

From Coq Require Import Relation_Operators.
From LC Require Import term a_equivalence tactics.

(* t ---> t': t evaluates to t' in one step                                   *)

(*   We adopt call-by-value.                                                  *)

Inductive eval_rel' : term -> term -> Prop :=
  E_App_1' : forall t1 t1' t2, eval_rel' t1 t1' -> eval_rel' (App t1 t2) (App t1' t2)
| E_App_2' : forall v1 t2 t2', is_value v1 -> eval_rel' t2 t2' -> eval_rel' (App v1 t2) (App v1 t2')
| E_Beta' : forall x T t11 v2, is_value v2 -> eval_rel' (App (Abs x T t11) v2) ([x \-> v2] t11)
| E_If' : forall t1 t1' t2 t3, eval_rel' t1 t1' -> eval_rel' (IfExp t1 t2 t3) (IfExp t1' t2 t3)
| E_If_Tru' : forall t2 t3, eval_rel' (IfExp Tru t2 t3) t2
| E_If_Fal' : forall t2 t3, eval_rel' (IfExp Fal t2 t3) t3.

#[export]
Hint Constructors eval_rel' : core.

Reserved Notation "t ---> t'" (at level 70).

Inductive eval_rel : term -> term -> Prop :=
  E_Alpha : forall t t'' t''' t', t =a t'' -> eval_rel' t'' t''' -> t''' =a t' -> t ---> t'
where "t ---> t'" := (eval_rel t t').

Lemma a_equiv_eval_rel : forall t s t' s',
  t =a s ->
  t' =a s' ->
  t ---> t' <-> s ---> s'.
Proof.
have H : forall t s t' s', t =a s -> t' =a s' -> t ---> t' -> s ---> s'.
  move=> t s t' s' aEts aEts' t_t'.
  case: t t' / t_t' aEts aEts' => t t'' t''' t' aEtt'' t''_t''' aEt'''t' aEts aEts'.
  apply: (@E_Alpha _ t'' t''' _); auto.
    apply: (rt_trans _ _ _ t); auto.
    by apply: a_equiv_sym.
  by apply: (rt_trans _ _ _ t').
move=> t s t' s' aEts aEts'.
move=> *; split.
  by apply: H.
by apply: H; apply: a_equiv_sym.
Qed.

Lemma E_App_1 t1 t1' t2 :
  t1 ---> t1' ->
  App t1 t2 ---> App t1' t2.
Proof.
case: t1 t1' / => [t1 t1'' t1''' t1' aE1 ev aE2].
apply: (@E_Alpha _ (App t1'' t2) (App t1''' t2)).
- by apply: AE_App_1.
- by apply: E_App_1'.
- by apply: AE_App_1.
Qed.

Lemma E_App_2 v1 t2 t2' (v1_is_val : is_value v1) :
  t2 ---> t2' ->
  App v1 t2 ---> App v1 t2'.
Proof.
case: t2 t2' / => [t2 t2'' t2''' t2' aE1 ev aE2].
apply: (@E_Alpha _ (App v1 t2'') (App v1 t2''')).
- by apply: AE_App_2.
- by apply: E_App_2'.
- by apply: AE_App_2.
Qed.

Lemma E_Beta x T t11 v2 (v2_is_val : is_value v2) :
  App (Abs x T t11) v2 ---> [x \-> v2] t11.
Proof.
by apply: (E_Alpha (rt_refl _ _ _) _ (rt_refl _ _ _)); apply: E_Beta'.
Qed.

Lemma E_If t1 t1' t2 t3 :
  t1 ---> t1' ->
  IfExp t1 t2 t3 ---> IfExp t1' t2 t3.
Proof.
case: t1 t1' / => [t1 t1'' t1''' t1' aE1 ev aE2].
apply: (@E_Alpha _ (IfExp t1'' t2 t3) (IfExp t1''' t2 t3)).
- by apply: AE_If_1.
- by apply: E_If'.
- by apply: AE_If_1.
Qed.

Lemma E_If_Tru t2 t3 :
  IfExp Tru t2 t3 ---> t2.
Proof.
by apply: (E_Alpha (rt_refl _ _ _) _ (rt_refl _ _ _)); apply: E_If_Tru'.
Qed.

Lemma E_If_Fal t2 t3 :
  IfExp Fal t2 t3 ---> t3.
Proof.
by apply: (E_Alpha (rt_refl _ _ _) _ (rt_refl _ _ _)); apply: E_If_Fal'.
Qed.
