From mathcomp Require Import all_ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

From Coq Require Import Relation_Operators.
From LC Require Import term substitution a_equivalence evaluation tactics.

(* G \|- t \: T: the term t has the type T in the typing context G            *)
(*     ud x T G: the typing context G' defined by G' x = T and                *)
(*               G' y = G y (y != x)                                          *)
(*                                                                            *)
(* Goal:                                                                      *)
(* 1. If [t =a t'], then [G \|- t \: T <-> G \|- t' \: T] (a_equiv_typing)    *)
(* 2. Substitution lemma (substitution_lemma). Since we introduced            *)
(*    a-equivalence, we can prove substitution lemma for any term (not only   *)
(*    closed terms).                                                          *)
(* 3. Type safety (preservation and progress)                                 *)

(*   Typing contexts are represented by partial functions from variables to   *)
(* types.                                                                     *)

Definition ctx := nat -> option ty.

Section ud.
Definition ud x T (G : ctx) :=
  fun y => if y == x then T else G y.

Lemma ud_same x v G :
  ud x v G x = v.
Proof. by rewrite /ud eq_refl. Qed.

Lemma ud_diff x v G y :
  y != x ->
  ud x v G y = G y.
Proof. by move=> /negbTE Hy; rewrite /ud Hy. Qed.

Lemma ud_twice x v1 v2 G :
  ud x v2 (ud x v1 G) =1 ud x v2 G.
Proof. by move=> y; rewrite /ud; case (y =P x). Qed.

Lemma ud_swap x v y w G :
  x != y ->
  ud x v (ud y w G) =1 ud y w (ud x v G).
Proof.
move=> x_neq_y z; rewrite /ud.
by case: (z =P x) => // ->; rewrite (negbTE x_neq_y).
Qed.

Lemma eq_ud x v G G' :
  (forall y, y != x -> G y = G' y) ->
  ud x v G =1 ud x v G'.
Proof.
move=> E y.
case: (y =P x) => [-> | /eqP ?].
  by rewrite !ud_same.
by rewrite !ud_diff // E.
Qed.
End ud.

Reserved Notation "G \|- x \: T" (at level 70).

Inductive typing : ctx -> term -> ty -> Prop :=
| Ty_Var : forall G x T, G x = Some T -> G \|- Var x \: T
| Ty_Abs : forall G x T1 t1 T2, ud x (Some T1) G \|- t1 \: T2 -> G \|- Abs x T1 t1 \: TyFun T1 T2
| Ty_App : forall G t1 t2 T1 T2, G \|- t1 \: TyFun T1 T2 -> G \|- t2 \: T1 -> G \|- App t1 t2 \: T2
| Ty_Tru : forall G, G \|- Tru \: TyBool
| Ty_Fal : forall G, G \|- Fal \: TyBool
| Ty_If : forall G t1 t2 t3 T, G \|- t1 \: TyBool -> G \|- t2 \: T -> G \|- t3 \: T -> G \|- IfExp t1 t2 t3 \: T
where "G \|- t \: T" := (typing G t T).

#[export]
Hint Constructors typing : core.

Theorem eq_typing : forall G G' t T,
  G =1 G' ->
  G \|- t \: T <-> G' \|- t \: T.
Proof.
have H : forall G G' t T, G =1 G' -> G \|- t \: T -> G' \|- t \: T.
  move=> G + t T + ty_t.
  elim: G t T / ty_t => // [G x T H | G x T1 t1 T2 _ IH | |] G' E; eauto.
    by apply: Ty_Var; rewrite -(E x).
  apply: Ty_Abs; apply: IH => y.
  case: (y =P x) => [-> | /eqP ?].
    by rewrite !ud_same.
  by rewrite !ud_diff.
by move=> *; split; apply: H.
Qed.

Lemma eq_typing_free_var : forall G G' t T,
    (forall x, x \in FV t -> G x = G' x) ->
    G \|- t \: T <-> G' \|- t \: T.
Proof.
have H : forall G G' t T,
    (forall x, x \in FV t -> G x = G' x) ->
    G \|- t \: T -> G' \|- t \: T.
  move=> G + t T + ty_t.
  elim: G t T / ty_t => // [G x T H | G x T1 t1 T2 H IH | G t1 t2 T1 T2 _ IH1 _ IH2 | G t1 t2 t3 T _ IH1 _ IH2 _ IH3] G' E.
  + by apply: Ty_Var; rewrite -(E x); solve_mem.
  + apply: Ty_Abs; apply: IH => y y_in_FV.
    case: (y =P x) => [-> | /eqP ?].
      by rewrite !ud_same.
    by rewrite !ud_diff // E; solve_mem.
  + by apply: Ty_App; [apply: IH1 | apply: IH2];
       move=> ??; apply: E; solve_mem.
  + by apply: Ty_If; [apply: IH1 | apply: IH2 | apply: IH3];
       move=> ??; apply: E; solve_mem.
move=> G G' t T E; split.
  by apply: H.
by apply: H; move=> *; symmetry; apply: E.
Qed.

Corollary infinite_ctx_is_not_necessary G t T :
  G \|- t \: T <->
  (fun x => if x \in FV t then G x else None) \|- t \: T.
Proof. by apply: eq_typing_free_var => ? ->. Qed.

Corollary typing_add_ud x v t G T :
  x \notin FV t ->
  G \|- t \: T -> ud x v G \|- t \: T.
Proof.
move=> x_notin_FV ty_t.
under eq_typing_free_var => y y_in_FV.
  rewrite ud_diff; last first.
    by apply/eqP => Eyx; rewrite -Eyx y_in_FV in x_notin_FV.
  over.
by [].
Qed.

Corollary typing_rem_ud x v t G T :
  x \notin FV t ->
  ud x v G \|- t \: T -> G \|- t \: T.
Proof.
move=> x_notin_FV.
under eq_typing_free_var => y y_in_FV.
  rewrite ud_diff; last first.
    by apply/eqP => Eyx; rewrite -Eyx y_in_FV in x_notin_FV.
  over.
by [].
Qed.

(*   The function [swap_keys] swaps the values of keys of typing contexts.    *)

Section swap_keys.
Definition swap_keys x y (G : ctx) :=
  ud x (G y) (ud y (G x) G).

Lemma swap_keysE x y G z :
  swap_keys x y G z = ud x (G y) (ud y (G x) G) z.
Proof. by []. Qed.

Lemma swap_keys_l x y G :
  swap_keys x y G x = G y.
Proof. by rewrite /swap_keys ud_same. Qed.

Lemma swap_keys_r x y G :
  swap_keys x y G y = G x.
Proof. 
rewrite /swap_keys.
case: (y =P x) => [-> | /eqP ?].
  by rewrite ud_same.
by rewrite ud_diff // ud_same.
Qed.

Lemma swap_keys_other x y G z :
  z != x -> z != y ->
  swap_keys x y G z = G z.
Proof. by move=> *; rewrite /swap_keys !ud_diff. Qed.
End swap_keys.

Lemma typing_subst x y t G T :
  G \|- t \: T ->
  y \notin FV t ->
  valid_subst x (Var y) t ->
  swap_keys x y G \|- {x \-> Var y} t \: T.
Proof.
elim: G t T / => // [G z T H /= | G z T1 t1 T2 H IH | ????????? | ???????????] y_notin_FV VS; try now (simpl; case_mem y_notin_FV; inversion VS; eauto).
  case: (z =P x) => [<- | /eqP ?]; apply: Ty_Var.
    by rewrite swap_keys_r.    
  by rewrite swap_keys_other; solve_mem.
inversion VS; subst.
- rewrite /= eq_refl.
  by rewrite /swap_keys; do ! apply: typing_add_ud; solve_mem.
- rewrite /= (negbTE H5).
  apply: Ty_Abs.
  under eq_typing => w.
    rewrite (_ : ud _ _ _ _ = swap_keys x y (ud z (Some T1) G) w); last first.
      rewrite /swap_keys /ud.
      case_mem H6 => H6.
      rewrite [y == _]eq_sym (negbTE H6) [x == _]eq_sym (negbTE H5).
      case: (w =P z) => // ->.
      by rewrite (negbTE H5) (negbTE H6).
    over.
  by apply: IH; solve_mem.
- rewrite notin_FV_subst_xst; solve_mem.
  by rewrite /swap_keys; do ! apply: typing_add_ud; solve_mem.
Qed.

Theorem a_equiv_typing : forall t t' G T,
  t =a t' ->
  G \|- t \: T <-> G \|- t' \: T.
Proof.
have H : forall t t' G T,
    t =a t' ->    
    G \|- t \: T -> G \|- t' \: T.
  move=> t t' ++ aEtt'.
  elim: t t' / aEtt' => [t t' aEtt' | |]; auto.
  elim: t t' / aEtt' => [x y T1 t1 y_notin_FV VS | ?????? | ????? | ????? | ?????? | ?????? | ??????] => G T ty_t; try now (inversion ty_t; subst; eauto).
  case: (x =P y) => [<- | /eqP x_neq_y].
    by rewrite subst_xxt.
  wlog : G ty_t / G y = G x.
    move=> H.
    apply: (@typing_rem_ud y (G x)); solve_mem; apply: H.
      by apply: typing_add_ud; solve_mem.
    by rewrite ud_same ud_diff.
  move=> EGy.
  inversion ty_t; subst.
  have := typing_subst H4 y_notin_FV VS.
  under eq_typing => z.
    rewrite (_ : swap_keys _ _ _ _ = ud y (Some T1) G z); last first.
      case: (z =P x) => [-> | /eqP z_neq_x].
        by rewrite swap_keys_l !ud_diff; solve_mem.
      case: (z =P y) => [-> | /eqP z_neq_y].
        by rewrite swap_keys_r !ud_same.
      by rewrite swap_keys_other // !ud_diff.
    over.
  by auto.
move=> t t' G T aEtt'; split.
  by apply: H.
by apply: H; auto; apply: a_equiv_sym.
Qed.

(* The substitution lemma is a bit strong in the sense that we do not require *)
(* that [s] is closed.                                                        *)

Lemma substitution_lemma x s t G T S :
  ud x (Some S) G \|- t \: T ->
  G \|- s \: S ->
  G \|- [x \-> s] t \: T.
Proof.
wlog : x s t / valid_subst x s t.
  move=> H.
  set t' := avoid_capture x s t.
  have VS_t' := valid_subst_avoid_capture x s t.
  under a_equiv_typing.
    rewrite UnderE; apply: (a_equiv_avoid_capture x s t).
  move=> ty_t' ty_s.
  under a_equiv_typing.
    rewrite (_ : [x \-> s] t = [x \-> s] t'); last first.
      by rewrite /capture_avoiding_subst [_ _ _ t']avoid_capture_id.
    over.
  by apply: H.
move=> VS_t; rewrite capture_avoiding_substE //.
elim: t VS_t G T => [y | y T1 t1 IH | t1 IH1 t2 IH2 | | | t1 IH1 t2 IH2 t3 IH3] VS_t G T /=.
- case: (y =P x) => [-> | /eqP y_neq_x].
    inversion 1; subst.
    by rewrite ud_same in H2; case: H2 => ->.
  inversion 1; subst.
  rewrite ud_diff // in H2.
  by move=> _; apply: Ty_Var.
- inversion VS_t; subst.
  + have y_notin_FV : y \notin FV (Abs y T1 t1) by solve_mem.
    move/(typing_rem_ud y_notin_FV).
    by rewrite eq_refl.
  + move=> ty_t ty_s.
    rewrite (negbTE H4).
    inversion ty_t; subst.
    apply: Ty_Abs; apply: IH; auto.
      by under eq_typing do rewrite ud_swap 1?eq_sym //.
    by apply: typing_add_ud.
  + have x_notin_FV : x \notin FV (Abs y T1 t1) by solve_mem.
    move/(typing_rem_ud x_notin_FV).
    by rewrite (negbTE H4) notin_FV_subst_xst //.
- by inversion VS_t; inversion 1; eauto.
- by inversion 1.
- by inversion 1.
- by inversion VS_t; inversion 1; eauto.
Qed.

Theorem preservation t t' G T :
  t ---> t' ->
  G \|- t \: T -> G \|- t' \: T.
Proof.
case: t t' / => [t t'' t''' t' aE1 ev aE2].
wlog : t t'' t''' t' aE1 ev aE2 / t = t'' /\ t''' = t'.
  move=> H.
  under a_equiv_typing do (rewrite UnderE; apply: aE1).
  move=> ty_t''.
  under a_equiv_typing do (rewrite UnderE; apply: a_equiv_sym; apply: aE2).
  by apply: (H _ _ _ _ (rt_refl _ _ t'') ev (rt_refl _ _ t''')).
case => -> <- {t t' aE1 aE2}.
elim: t'' t''' / ev G T => [t1 t1' t2 _ IH | v1 t2 t2' Hv1 _ IH | x T' t11 v2 Hv2 | t1 t1' t2 t3 _ IH | t2 t3 | t2 t3] G T ty_t; try now (inversion ty_t; eauto).
inversion ty_t; subst.
inversion H2; subst.
by apply: (substitution_lemma H1 H4).
Qed.

Theorem progress t T :
  (fun x => None) \|- t \: T ->
  (exists t', t ---> t') \/ is_value t.
Proof.
move EG : (fun x => None) => G ty_t.
elim: G t T / ty_t EG => [???? | ??????? | G t1 t2 T1 T2 H1 IH1 _ IH2 | | | G t1 t2 t3 T H1 IH1 _ IH2 _ IH3] ?; subst.
- by discriminate.
- by right.
- case: (IH1 erefl) => [[t1' t1_t1'] | t1_is_val].
    by left; exists (App t1' t2); apply: E_App_1.
  inversion t1_is_val; subst; inversion H1; subst.
  case: (IH2 erefl) => [[t2' t2_t2'] | t2_is_val].
    by left; exists (App (Abs x T1 t0) t2'); apply: E_App_2.
  by left; exists ([x \-> t2] t0); apply: E_Beta.
- by right.
- by right.
- case: (IH1 erefl) => [[t1' t1_t1'] | t1_is_val].
    by left; exists (IfExp t1' t2 t3); apply: E_If.
  inversion t1_is_val; subst; inversion H1; subst.
    by left; exists t2; apply: E_If_Tru.
  by left; exists t3; apply: E_If_Fal.
Qed.
