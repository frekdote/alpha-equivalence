# alpha-equivalence
A formalization of Simply Typed Lambda Calculus *with α-equivalence* using Coq.

## Organization
- `term.v`: term (before equating α-equivalent terms)
- `substitution.v`: naive substitution (that may cause variable capture)
- `a_equivalence.v`: α-equivalence, capture-avoiding substitution
- `evaluation.v`: evaluation relation
- `typing.v`: type system
- `tactics.v`: tactics reasoning about variables
- `bigop_ext.v`: lemmas for bigop

## Goal
1. Prove the equivalence of two definitions of α-equivalence: `=a` (a definition in [1]) and `==a` (another one in [2]).
2. Prove that substitution preserves α-equivalence.
3. Prove that every substitution can be converted to capture-avoiding one by renaming bound variables in its components.
4. Define capture-avoiding substitution. (2) and (3) justify this definition.
5. Prove substitution lemma. Since we introduce α-equivalence, we can prove substitution lemma for any term (not only closed terms).
6. Prove type safety.

## References
- [1] Morten Heine Sørensen and Pawel Urzyczyn. Lectures on the Curry-Howard Isomorphism. Vol. 149. Studies in Logic and the Foundations of Mathematics. Elsevier, 2006.
- [2] Yuichi Komori and Hiroakira Ono. 現代数理論理学序説. 日本評論社, 2010 (in Japanese).
- [3] Benjamin C. Pierce et al. [Programming Language Foundations](https://softwarefoundations.cis.upenn.edu/plf-current/index.html). Software Foundations series, volume 2.
