From mathcomp Require Import all_ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

From LC Require Import term.

(*   The file provides three tactics:                                         *)
(* -      case_mem: Performs case analysis on x \in Vars t, x \in FV t, and   *)
(*                  x \in BV t                                                *)
(* - deep_case_mem: Performs more detailed case analysis than case_mem        *)
(* -     solve_mem: Solves goals on x \in Vars t, x \in FV t, and x \in BV t  *)

Tactic Notation "case_mem" :=
  let H := fresh in
  move=> H;
  match type of H with
  | is_true (_ \in (Vars (Var _))) => (* Vars *)
    rewrite in_Vars_Var in H; move: H
  | is_true (_ \in (Vars (Abs _ _ _))) =>
    rewrite in_Vars_Abs in H; case/orP: H
  | is_true (_ \in (Vars (App _ _))) =>
    rewrite in_Vars_App in H; case/orP: H
  | is_true (_ \in (Vars Tru)) => (* contradiction *)
    rewrite in_Vars_Tru // in H
  | is_true (_ \in (Vars Fal)) => (* contradiction *)
    rewrite in_Vars_Fal // in H
  | is_true (_ \in (Vars (IfExp _ _ _))) =>
    rewrite in_Vars_If in H; case/orP: H; [case/orP |]
  | is_true (_ \notin (Vars (Var _))) =>
    rewrite in_Vars_Var in H; move: H
  | is_true (_ \notin (Vars (Abs _ _ _))) =>
    rewrite in_Vars_Abs in H; case/norP: H
  | is_true (_ \notin (Vars (App _ _))) =>
    rewrite in_Vars_App in H; case/norP: H
  | is_true (_ \notin (Vars Tru)) => (* H is meaningless *)
    clear H
  | is_true (_ \notin (Vars Fal)) => (* H is meaningless *)
    clear H
  | is_true (_ \notin (Vars (IfExp _ _ _))) =>
    rewrite in_Vars_If in H; case/norP: H; case/norP
  | is_true (_ \in (FV (Var _))) => (* FV *)
    rewrite in_FV_Var in H; move: H
  | is_true (_ \in (FV (Abs _ _ _))) =>
    rewrite in_FV_Abs in H; case/andP: H
  | is_true (_ \in (FV (App _ _))) =>
    rewrite in_FV_App in H; case/orP: H
  | is_true (_ \in (FV Tru)) => (* contradiction *)
    rewrite in_FV_Tru // in H
  | is_true (_ \in (FV Fal)) => (* contradiction *)
    rewrite in_FV_Fal // in H
  | is_true (_ \in (FV (IfExp _ _ _))) =>
    rewrite in_FV_If in H; case/orP: H; [case/orP |]
  | is_true (_ \notin (FV (Var _))) =>
    rewrite in_FV_Var in H; move: H
  | is_true (_ \notin (FV (Abs _ _ _))) =>
    rewrite notin_FV_Abs in H; case/orP: H; [| case/andP]
  | is_true (_ \notin (FV (App _ _))) =>
    rewrite in_FV_App in H; case/norP: H
  | is_true (_ \notin (FV Tru)) => (* H is meaningless *)
    clear H
  | is_true (_ \notin (FV Fal)) => (* H is meaningless *)
    clear H
  | is_true (_ \notin (FV (IfExp _ _ _))) =>
    rewrite in_FV_If in H; case/norP: H; case/norP
  | is_true (_ \in (BV (Var _))) => (* BV *)
    rewrite in_BV_Var // in H (* contradiction *)
  | is_true (_ \in (BV (Abs _ _ _))) =>
    rewrite in_BV_Abs in H; case/orP: H
  | is_true (_ \in (BV (App _ _))) =>
    rewrite in_BV_App in H; case/orP: H
  | is_true (_ \in (BV Tru)) => (* contradiction *)
    rewrite in_BV_Tru // in H
  | is_true (_ \in (BV Fal)) => (* contradiction *)
    rewrite in_BV_Fal // in H
  | is_true (_ \in (BV (IfExp _ _ _))) =>
    rewrite in_BV_If in H; case/orP: H; [case/orP |]
  | is_true (_ \notin (BV (Var _))) =>
    clear H (* H is meaningless *)
  | is_true (_ \notin (BV (Abs _ _ _))) =>
    rewrite in_BV_Abs in H; case/norP: H
  | is_true (_ \notin (BV (App _ _))) =>
    rewrite in_BV_App in H; case/norP: H
  | _ => fail
  | is_true (_ \notin (BV Tru)) => (* H is meaningless *)
    clear H
  | is_true (_ \notin (BV Fal)) => (* H is meaningless *)
    clear H
  | is_true (_ \notin (BV (IfExp _ _ _))) =>
    rewrite in_BV_If in H; case/norP: H; case/norP
  end.

Tactic Notation "case_mem" hyp(H) := move: H; case_mem.

Local Ltac deep_case_mem' :=
  let H := fresh in
  let H' := fresh in
  move=> H;
  move: (H) => H';
  move: H';
  match type of H with
  | is_true (_ \in (_ (Var _))) =>
    case_mem H
  | is_true (_ \notin (_ (Var _))) =>
    case_mem H
  | is_true (_ \in (_ (App _ _))) =>
    case_mem H; try deep_case_mem'
  | is_true (_ \notin (_ (App _ _))) =>
    let H1 := fresh in
    let H2 := fresh in
    case_mem H => H1 H2;
    move: H2;
    try deep_case_mem';
    move: H1;
    try deep_case_mem'
  | is_true (_ \in (_ Tru)) =>
    case_mem H
  | is_true (_ \notin (_ Tru)) =>
    case_mem H
  | is_true (_ \in (_ Fal)) =>
    case_mem H
  | is_true (_ \notin (_ Fal)) =>
    case_mem H
  | is_true (_ \in (_ (IfExp _ _ _))) =>
    case_mem H; try deep_case_mem'
  | is_true (_ \notin (_ (IfExp _ _ _))) =>
    let H1 := fresh in
    let H2 := fresh in
    let H3 := fresh in
    case_mem H => H1 H2 H3;
    move: H3;
    try deep_case_mem';
    move: H2;
    try deep_case_mem';    
    move: H1;
    try deep_case_mem'
  | is_true (_ \in (FV (Abs _ _ _))) =>
    let H1 := fresh in
    let H2 := fresh in
    case_mem H => H1 H2;
    move: H2;
    try deep_case_mem';
    move: H1
  | is_true (_ \notin (FV (Abs _ _ _))) =>
    let H1 := fresh in
    let H2 := fresh in
    case_mem H => [| H1 H2];
    [| move: H2; try deep_case_mem'; move: H1]
  | is_true (_ \in (_ (Abs _ _ _))) =>
    case_mem H; try deep_case_mem'
  | is_true (_ \notin (_ (Abs _ _ _))) =>
    let H1 := fresh in
    let H2 := fresh in
    case_mem H => H1 H2;
    move: H2;
    try deep_case_mem';
    move: H1
  | _ => fail
  end.

Tactic Notation "deep_case_mem" := deep_case_mem'.
Tactic Notation "deep_case_mem" hyp(H) := move: H; deep_case_mem.

Definition in_VarsE := (in_Vars_Var, in_Vars_Abs, in_Vars_App, in_Vars_Tru, in_Vars_Fal, in_Vars_If, in_FV_Var, in_FV_Abs, in_FV_App, in_FV_Tru, in_FV_Fal, in_FV_If, in_BV_Var, in_BV_Abs, in_BV_App, in_BV_Tru, in_BV_Fal, in_BV_If, negb_or, negb_and).

Ltac solve_mem' :=
  move=> * //;
  (* 1. Decompose all hypotheses on membership *)
  repeat match goal with
         | H : is_true (_ \in _ _) |- _ =>
           deep_case_mem H
         | H : is_true (_ \notin _ _) |- _ =>
           deep_case_mem H
         end;
  move=> * //;
  (* 2. Derive new hypotheses *)
  repeat match goal with
         | H : ?P |- ?P => apply: H
         | H : ?P, H' : ?P -> ?Q |- _ => specialize (H' H)
         end;
  (* 3. Decompose the goal *)
  repeat match goal with
         | |- context [_ \in (Vars (Var _))] => (* Vars *)
           rewrite in_Vars_Var
         | |- context [_ \in (Vars (Abs _ _ _))] =>
           rewrite in_Vars_Abs
         | |- context [_ \in (Vars (App _ _))] =>
           rewrite in_Vars_App
         | |- context [_ \in (Vars (IfExp _ _ _))] =>
           rewrite in_Vars_If
         | |- context [_ \notin (Vars (Var _))] =>
           rewrite in_Vars_Var
         | |- context [_ \notin (Vars (Abs _ _ _))] =>
           rewrite in_Vars_Abs
         | |- context [_ \notin (Vars (App _ _))] =>
           rewrite in_Vars_App
         | |- context [_ \notin (Vars Tru)] =>
           rewrite /=
         | |- context [_ \notin (Vars Fal)] =>
           rewrite /=
         | |- context [_ \notin (Vars (IfExp _ _ _))] =>
           rewrite in_Vars_If
         | |- context [_ \in (FV (Var _))] => (* FV *)
           rewrite in_FV_Var
         | |- context [_ \in (FV (Abs _ _ _))] =>
           rewrite in_FV_Abs
         | |- context [_ \in (FV (App _ _))] =>
           rewrite in_FV_App
         | |- context [_ \in (FV (IfExp _ _ _))] =>
           rewrite in_FV_If
         | |- context [_ \notin (FV (Var _))] =>
           rewrite in_FV_Var
         | |- context [_ \notin (FV (Abs _ _ _))] =>
           rewrite notin_FV_Abs
         | |- context [_ \notin (FV (App _ _))] =>
           rewrite in_FV_App
         | |- context [_ \notin (FV Tru)] =>
           rewrite /=
         | |- context [_ \notin (FV Fal)] =>
           rewrite /=
         | |- context [_ \notin (FV (IfExp _ _ _))] =>
           rewrite in_FV_If
         | |- context [_ \in (BV (Abs _ _ _))] => (* BV *)
           rewrite in_BV_Abs
         | |- context [_ \in (BV (App _ _))] =>
           rewrite in_BV_App
         | |- context [_ \in (BV (IfExp _ _ _))] =>
           rewrite in_BV_If
         | |- context [_ \notin (BV (Var _))] =>
           rewrite /=
         | |- context [_ \notin (BV (Abs _ _ _))] =>
           rewrite in_BV_Abs
         | |- context [_ \notin (BV (App _ _))] =>
           rewrite in_BV_App
         | |- context [_ \notin (BV Tru)] =>
           rewrite /=
         | |- context [_ \notin (BV Fal)] =>
           rewrite /=
         | |- context [_ \notin (BV (IfExp _ _ _))] =>
           rewrite in_BV_If
         end;
  (* 4. Rewrite the goal using hypotheses *)
  repeat match goal with
         | |- context [?x == ?x] =>
           rewrite eq_refl //=
         | H : is_true (?x != ?x) |- _ =>
           rewrite eq_refl // in H
         | H : is_true ?x |- context [?x] =>
           rewrite H //=
         | H : is_true (~~ ?x) |- context [?x] =>
           rewrite (negbTE H) //=
         | H : is_true (?x == ?y) |- context [?y == ?x] =>
           let H' := fresh in
           move: (H) => H';
           rewrite eq_sym in H';
           rewrite H' //=;
           clear H'
         | H : is_true (?x != ?y) |- context [?y == ?x] =>
           let H' := fresh in
           move: (H) => /negbTE H';
           rewrite eq_sym in H';
           rewrite H' //=;
           clear H'
         | H : is_true ?x, H' : is_true (~~ ?x) |- _ =>
           rewrite H // in H'
         | H : is_true (?x == ?y), H' : is_true (?y != ?x) |- _ =>
           rewrite eq_sym H // in H'
         | H : is_true (?x \notin Vars ?y) |- context [?x \in FV ?y] =>
           let H' := fresh in
           move: (H) => H';
           rewrite in_Vars_FV_V_BV in H';
           case/norP: H' => /negbTE -> _ //=
         | H : is_true (?x \notin Vars ?y) |- context [?x \in BV ?y] =>
           let H' := fresh in
           move: (H) => H';
           rewrite in_Vars_FV_V_BV in H';
           case/norP: H' => _ /negbTE -> //=
         | _ => rewrite ?(orbT, andbF) //=; auto
         end.

Tactic Notation "solve_mem" := try now solve_mem'.
