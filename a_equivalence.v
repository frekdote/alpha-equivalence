From mathcomp Require Import all_ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

From Coq Require Import Relation_Operators Setoid.
From LC Require Import term substitution tactics bigop_ext.

(*   We use two equivalent definitions of a-equivalence [=a] and [==a].       *)
(* [t =a t'] means that [t'] can be obtained by renaming bound variables      *)
(* in [t]. [t ==a t'] means that you can rename each bound variable in [t]    *)
(* and the corresponding one in [t'] to the same variable so that             *)
(* [t] and [t'] coincide.                                                     *)
(*                                                                            *)
(* Example:                                                                   *)
(*   Let [a], [b], [c], [d], [e], and [f] be distinct variables.              *)
(* - We obtain [\a. \b. ab =a \c. \d. cd] as follows:                         *)
(*   (1) We rename [a] in [\a. \b. ab] to [c], then we obtain [\c. \b. cb].   *)
(*   (2) We rename [b] in [\c. \b. cb] to [d], then we obtain [\c. \d. cd].   *)
(* - We obtain [\a. \b. ab ==a \c. \d. cd] as follows:                        *)
(*   (1) We rename [a] in [\a. \b. ab] and [c] in [\c. \d. cd] to [e],        *)
(*       then we obtain [\e. \b. eb] and [\e. \d. ed].                        *)
(*   (2) We rename [b] in [\e. \b. eb] and [d] in [\e. \d. ed] to [f],        *)
(*       then they become the same term [\e. \f. ef].                         *)
(*                                                                            *)
(*   We mainly use the first definition [=a]. The second definition [==a] is  *)
(* used to prove [a_equiv_subst]. Although we can prove the theorem without   *)
(* [==a], even in that case we must prove that [=a] has a property similar to *)
(* that of [==a] (see the bottom). Hence we use [==a]. The latter part of the *)
(* proof strategy is based on [2].                                            *)
(*                                                                            *)
(* [x \-> s] t: the term obtained by replacing all free occurrences of x in t *)
(*              with s and, if necessary, renaming bound variables in t to    *)
(*              avoid variable capture                                        *)
(*                                                                            *)
(* Goal:                                                                      *)
(* 1. The equivalence of [=a] and [==a]. (a_equiv_iff)                        *)
(* 2. Substitution preserves a-equivalence (a_equiv_subst):                   *)
(*    For all [x], [s], [s'], [t], [t'], if they satisfy the following        *)
(*    conditions, then [{x \-> s} t] and [{x \-> s'} t'] are a-equivalent.    *)
(*    (1) [s] and [s'] are a-equivalent.                                      *)
(*    (2) [t] and [t'] are a-equivalent.                                      *)
(*    (3) [{x \-> s} t] and [{x \-> s'} t'] are capture-avoiding.             *)
(* 3. Every substitution can be converted to capture-avoiding one by renaming *)
(*    bound variables in its components. More precisely, there exists a term  *)
(*    [t'] such that [t =a t'] and [{x \-> s} t'] is capture-avoiding for all *)
(*    [x], [s], [t]. (a_equiv_avoid_capture and valid_subst_avoid_capture)    *)
(* 4. Capture-avoiding substitution [[x \-> s] t]. (2) and (3) justify        *)
(*    the definition.                                                         *)

Reserved Notation "t =a1 t'" (at level 70).

Inductive a_equiv_1 : term -> term -> Prop :=
  AE1_Rename : forall x y T t1, y \notin FV t1 -> valid_subst x (Var y) t1 -> Abs x T t1 =a1 Abs y T ({x \-> Var y} t1)
| AE1_Abs : forall x T t1 t1', t1 =a1 t1' -> Abs x T t1 =a1 Abs x T t1'
| AE1_App_1 : forall t1 t1' t2, t1 =a1 t1' -> App t1 t2 =a1 App t1' t2
| AE1_App_2 : forall t1 t2 t2', t2 =a1 t2' -> App t1 t2 =a1 App t1 t2'
| AE1_If_1 : forall t1 t1' t2 t3, t1 =a1 t1' -> IfExp t1 t2 t3 =a1 IfExp t1' t2 t3
| AE1_If_2 : forall t1 t2 t2' t3, t2 =a1 t2' -> IfExp t1 t2 t3 =a1 IfExp t1 t2' t3
| AE1_If_3 : forall t1 t2 t3 t3', t3 =a1 t3' -> IfExp t1 t2 t3 =a1 IfExp t1 t2 t3'
where "t =a1 t'" := (a_equiv_1 t t').

#[export]
Hint Constructors a_equiv_1 : core.

Definition a_equiv := clos_refl_trans term a_equiv_1.

Notation "t =a t'" := (a_equiv t t') (at level 70).

#[export]
Hint Unfold a_equiv : core.

Lemma AE_Abs x T t1 t1' :
  t1 =a t1' -> Abs x T t1 =a Abs x T t1'.
Proof.
elim: t1 t1' / => [* | * | ??? _ IH1 _ IH2].
- by apply: rt_step; apply: AE1_Abs.
- by apply: rt_refl.
- by apply: rt_trans IH1 IH2.
Qed.

Lemma AE_App_1 t1 t1' t2 :
  t1 =a t1' -> App t1 t2 =a App t1' t2.
Proof.
elim: t1 t1' / => [* | * | ??? _ IH1 _ IH2].
- by apply: rt_step; apply: AE1_App_1.
- by apply: rt_refl.
- by apply: rt_trans IH1 IH2.
Qed.

Lemma AE_App_2 t1 t2 t2' :
  t2 =a t2' -> App t1 t2 =a App t1 t2'.
Proof.
elim: t2 t2' / => [* | * | ??? _ IH1 _ IH2].
- by apply: rt_step; apply: AE1_App_2.
- by apply: rt_refl.
- by apply: rt_trans IH1 IH2.
Qed.  

Lemma AE_App t1 t2 t1' t2' :
  t1 =a t1' -> t2 =a t2' ->
  App t1 t2 =a App t1' t2'.
Proof.
move=> *.
apply: (rt_trans _ _ _ (App t1' t2) _).
  by apply: AE_App_1.
by apply: AE_App_2.
Qed.

Lemma AE_If_1 t1 t1' t2 t3 :
  t1 =a t1' -> IfExp t1 t2 t3 =a IfExp t1' t2 t3.
Proof.
elim: t1 t1' / => [* | * | ??? _ IH1 _ IH2].
- by apply: rt_step; apply: AE1_If_1.
- by apply: rt_refl.
- by apply: rt_trans IH1 IH2.
Qed.

Lemma AE_If_2 t1 t2 t2' t3 :
  t2 =a t2' -> IfExp t1 t2 t3 =a IfExp t1 t2' t3.
Proof.
elim: t2 t2' / => [* | * | ??? _ IH1 _ IH2].
- by apply: rt_step; apply: AE1_If_2.
- by apply: rt_refl.
- by apply: rt_trans IH1 IH2.
Qed.

Lemma AE_If_3 t1 t2 t3 t3' :
  t3 =a t3' -> IfExp t1 t2 t3 =a IfExp t1 t2 t3'.
Proof.
elim: t3 t3' / => [* | * | ??? _ IH1 _ IH2].
- by apply: rt_step; apply: AE1_If_3.
- by apply: rt_refl.
- by apply: rt_trans IH1 IH2.
Qed.

Lemma AE_If t1 t2 t3 t1' t2' t3' :
  t1 =a t1' -> t2 =a t2' -> t3 =a t3' ->
  IfExp t1 t2 t3 =a IfExp t1' t2' t3'.
Proof.
move=> *.
apply: (rt_trans _ _ _ (IfExp t1' t2 t3) _).
  by apply: AE_If_1.
apply: (rt_trans _ _ _ (IfExp t1' t2' t3) _).
  by apply: AE_If_2.
by apply: AE_If_3.
Qed.

#[export]
Hint Resolve AE_Abs AE_App_1 AE_App_2 AE_App AE_If_1 AE_If_2 AE_If_3 AE_If : core.

Theorem a_equiv_sym t t' :
  t =a t' -> t' =a t.
Proof.
elim: t t' / => [t t' | * | ??? _ IH1 _ IH2].
- elim: t t' / => [x y T t1 Hy HVS | | | | | |]; auto.
  case: (x =P y) => [-> | /eqP Hxy].
    by rewrite subst_xxt; apply: rt_refl.
  rewrite -{2}(subst_yx_xy_t Hy HVS).
  apply: rt_step; apply: AE1_Rename.
    by apply: neq_notin_FV_subst.
  by apply: valid_subst_yx_xy_t.
- by apply: rt_refl.
- by apply: rt_trans IH2 IH1.
Qed.

#[export]
Instance a_equiv_rr : RewriteRelation a_equiv := {}.

#[export]
Instance a_equiv_equiv : Equivalence a_equiv.
Proof.
split.
- by apply: rt_refl.
- by apply: a_equiv_sym.
- by apply: rt_trans.
Qed.

Reserved Notation "t ==a t'" (at level 70).

Inductive a_equiv' : term -> term -> Prop :=
  AE'_Refl : forall t, t ==a t
| AE'_Abs : forall z x T t1 y t1', z \notin Vars (Abs x T t1) -> z \notin Vars (Abs y T t1') -> {x \-> Var z} t1 ==a {y \-> Var z} t1' -> Abs x T t1 ==a Abs y T t1'
| AE'_App : forall t1 t2 t1' t2', t1 ==a t1' -> t2 ==a t2' -> App t1 t2 ==a App t1' t2'
| AE'_If : forall t1 t2 t3 t1' t2' t3', t1 ==a t1' -> t2 ==a t2' -> t3 ==a t3' -> IfExp t1 t2 t3 ==a IfExp t1' t2' t3'
where "t ==a t'" := (a_equiv' t t').

#[export]
Hint Constructors a_equiv' : core.

(*   In order to prove the transitivity of [==a], we need some functions.     *)

Section Renaming.
Variable f : nat -> nat.

(* rename_vars f t: the term obtained by replacing all variables x in t       *)
(*                  with f x                                                  *)
Fixpoint rename_vars t :=
  match t with
    Var x => Var (f x)
  | Abs x T t1 => Abs (f x) T (rename_vars t1)
  | App t1 t2 => App (rename_vars t1) (rename_vars t2)
  | Tru => Tru
  | Fal => Fal
  | IfExp t1 t2 t3 => IfExp (rename_vars t1) (rename_vars t2) (rename_vars t3)
  end.

Lemma rename_vars_Var x :
  rename_vars (Var x) = Var (f x).
Proof. by []. Qed.

Lemma rename_vars_Abs x T t1 :
  rename_vars (Abs x T t1) = Abs (f x) T (rename_vars t1).
Proof. by []. Qed.

Lemma Vars_rename_vars t :
  Vars (rename_vars t) = map f (Vars t).
Proof.
by elim: t => // [??? | ?+? | ?+?+?]; rewrite /Vars /=;
   repeat move->; rewrite ?map_cat.
Qed.

Hypothesis f_bij : bijective f.

Lemma rename_vars_subst x s t :
  rename_vars ({x \-> s} t) = {f x \-> rename_vars s} (rename_vars t).
Proof.
elim: t => //= [y | y * | * | *].
- by rewrite bij_eq; case: (y =P x).
- by rewrite bij_eq; case: (y =P x) => //= _; congr Abs.
- by congr App.
- by congr IfExp.
Qed.

(*   Renaming variables preserves a-equivalence.                              *)

Lemma a_equiv'_rename_vars t t' :
  t ==a t' ->
  rename_vars t ==a rename_vars t'.
Proof.
elim: t t' / => //= [z x t1 y t1' * | |]; auto.
apply: (@AE'_Abs (f z)).
1-2: by rewrite -rename_vars_Abs Vars_rename_vars mem_map //;
        apply: bij_inj.
by rewrite -!rename_vars_Var -!rename_vars_subst.
Qed.
End Renaming.

Section SwappingNats.
Definition swap_nats (n m x : nat) :=
  if x == n then m
  else if x == m then n
       else x.

Lemma swap_nats_l n m :
  (swap_nats n m) n = m.
Proof. by rewrite /swap_nats eq_refl. Qed.

Lemma swap_nats_r n m :
  (swap_nats n m) m = n.
Proof. by rewrite /swap_nats eq_refl; case: (m =P n). Qed.

Lemma neq_swap_nats n m x :
  x != n -> x != m ->
  (swap_nats n m) x = x.
Proof. by rewrite /swap_nats; move=> /negbTE-> /negbTE->. Qed.

Lemma swap_nats_bij n m :
  bijective (swap_nats n m).
Proof.
apply: inv_bij => x.
case: (x =P n) => [<- | /eqP ?];
  case: (x =P m) => [<- | /eqP ?];
    rewrite ?(swap_nats_l, swap_nats_r) //.
by rewrite ![_ _ _ x]neq_swap_nats.
Qed.

Lemma rename_vars_swap_nats n m t :
  n \notin Vars t ->
  m \notin Vars t ->
  rename_vars (swap_nats n m) t = t.
Proof.
elim: t => //= [x | ???? | ???? | ??????] ??.
- by rewrite neq_swap_nats; solve_mem.
- congr Abs; solve_mem.
    by rewrite neq_swap_nats; solve_mem.
- congr App; solve_mem.
- congr IfExp; solve_mem.
Qed.
End SwappingNats.

(*   The lemma [a_equiv'_Abs] is very important.                              *)

Lemma a_equiv'_Abs x T t1 y t1' :
  Abs x T t1 ==a Abs y T t1' ->
  forall z, z \notin Vars (Abs x T t1) ->
            z \notin Vars (Abs y T t1') ->
            {x \-> Var z} t1 ==a {y \-> Var z} t1'.
Proof.
inversion 1; subst; auto.
move=> w Hw Hw'.
have ? := swap_nats_bij z w.
rewrite -(@swap_nats_l z w) -rename_vars_Var.
rewrite -(@neq_swap_nats z w x); solve_mem.
rewrite -(@neq_swap_nats z w y); solve_mem.
rewrite -(@rename_vars_swap_nats z w t1); solve_mem.
rewrite -(@rename_vars_swap_nats z w t1'); solve_mem.
rewrite -!rename_vars_subst //.
by apply: a_equiv'_rename_vars.
Qed.

Theorem a_equiv'_trans t t' t'' :
  t ==a t' -> t' ==a t'' -> t ==a t''.
Proof.
move Heqsi: (size t) => si.
elim/ltn_ind: si t t' t'' Heqsi => si IH t t' t'' ? aEtt' aEt't''; subst.
inversion aEtt'; subst; auto.
- clear z H H0 H1.
  inversion aEt't''; subst; auto.
  clear z H2 H4 H5.
  rename y0 into z.
  rename t1'0 into t1''.
  have [w Hw] := existence_of_fresh_var (App (Abs x T t1) (App (Abs y T t1') (Abs z T t1''))).
  apply: (@AE'_Abs w); solve_mem.
  apply: (IH (size ({x \-> Var w} t1)) _ _ ({y \-> Var w} t1') _).
  + by rewrite size_subst_xyt.
  + by rewrite size_subst_xyt.
  + by apply: (a_equiv'_Abs aEtt'); solve_mem.
  + by apply: (a_equiv'_Abs aEt't''); solve_mem.
- inversion aEt't''; subst; auto.
  apply: AE'_App.
    apply: (IH (size t1) _ _ t1' _); auto.
    by rewrite /= ltnS leq_addr.
  apply: (IH (size t2) _ _ t2' _); auto.
  by rewrite /= ltnS leq_addl.  
- inversion aEt't''; subst; auto.
  apply: AE'_If.
  + apply: (IH (size t1) _ _ t1' _); auto.
    by rewrite /= ltnS -addnA leq_addr.
  + apply: (IH (size t2) _ _ t2' _); auto.
    by rewrite /= ltnS addnAC leq_addl.
  + apply: (IH (size t3) _ _ t3' _); auto.
    by rewrite /= ltnS leq_addl.
Qed.

(*   Next, we show the equivalence of [=a] and [==a].                         *)

Theorem a_equiv'_subst_abt a b t t' :
  t ==a t' ->
  b \notin Vars t ->
  b \notin Vars t' ->
  {a \-> Var b} t ==a {a \-> Var b} t'.
Proof.
move Heqsi: (size t) => si.
elim/ltn_ind: si t t' Heqsi => si IH t t' ? aEtt' Hb1 Hb2; subst.
inversion aEtt'; subst; auto.
- clear z H H0 H1.
  have [z Hz] := existence_of_fresh_var (App (Var a) (App (Var b) (App (Abs x T t1) (Abs y T t1')))).
  have aEsubst : {a \-> Var b} {x \-> Var z} t1 ==a {a \-> Var b} {y \-> Var z} t1'.
    apply: (IH (size ({x \-> Var z} t1))); auto.
    * by rewrite size_subst_xyt.
    * by apply: (a_equiv'_Abs aEtt'); solve_mem.
    * by apply: notin_Vars_notin_Vars_subst; solve_mem.
    * by apply: notin_Vars_notin_Vars_subst; solve_mem.
  simpl.
  case: (x =P a) => /= [? | /eqP Exa]; subst.
    case: (y =P a) => // /eqP Eya.
    rewrite subst_xy_xz_t 1?subst_ab_cd_t in aEsubst; solve_mem.
    apply: (@AE'_Abs z); solve_mem.
    by rewrite !in_VarsE notin_Vars_notin_Vars_subst; solve_mem.
  case: (y =P a) => [? | /eqP Eya]; subst.
    rewrite subst_xy_xz_t 1?subst_ab_cd_t in aEsubst; solve_mem.
    apply: (@AE'_Abs z); solve_mem.
    by rewrite !in_VarsE notin_Vars_notin_Vars_subst; solve_mem.
  rewrite ![{a \-> _} {_ \-> _} _]subst_ab_cd_t in aEsubst; solve_mem.
  by apply: (@AE'_Abs z); rewrite ?in_VarsE ?notin_Vars_notin_Vars_subst; solve_mem.
- simpl.
  apply: AE'_App.
    apply: (IH (size t1)); solve_mem.
    by rewrite /= ltnS leq_addr.
  apply: (IH (size t2)); solve_mem.
  by rewrite /= ltnS leq_addl.
- simpl.
  apply: AE'_If.
  + apply: (IH (size t1)); solve_mem.
    by rewrite /= ltnS -addnA leq_addr.
  + apply: (IH (size t2)); solve_mem.
    by rewrite /= ltnS addnAC leq_addl.
  + apply: (IH (size t3)); solve_mem.
    by rewrite /= ltnS leq_addl.
Qed.

Lemma AE'_Abs' x T t1 t1' :
  t1 ==a t1' -> Abs x T t1 ==a Abs x T t1'.
Proof.
move=> ?.
have [y Hy] := existence_of_fresh_var (App (Abs x T t1) (Abs x T t1')).
apply: (@AE'_Abs y); solve_mem.
by apply: a_equiv'_subst_abt; solve_mem.
Qed.

Theorem a_equiv_iff t t' :
  t =a t' <-> t ==a t'.
Proof.
split.
  elim: t t' / => [t t' aEtt' | t | t t'' t' _ IH1 _ IH2].
  + elim: t t' / aEtt' => [x y T t1 Hy HVS | x T t1 t1' _ IH | | | | |]; auto.
      have [z Hz] := existence_of_fresh_var (App (Abs x T t1) (Abs y T ({x \-> Var y} t1))).
      apply: (@AE'_Abs z); solve_mem.
      by rewrite subst_yz_xy_t; solve_mem.
    by apply: AE'_Abs'.
  + by apply: AE'_Refl.
  + by apply: a_equiv'_trans IH1 IH2.
elim: t t' / => [t | z x T t1 y t1' Hz1 Hz2 _ IH | t1 t2 t1' t2' _ IH1 _ IH2 | t1 t2 t3 t1' t2' t3' _ IH1 _ IH2 _ IH3].
- by apply: rt_refl.
- apply: (rt_trans _ _ _ (Abs z T ({x \-> Var z} t1)) _).
    apply: rt_step; apply: AE1_Rename; solve_mem.
    by apply: notin_BV_valid_subst; solve_mem.
  apply: (rt_trans _ _ _ (Abs z T ({y \-> Var z} t1')) _).
    by apply: AE_Abs.
  apply: a_equiv_sym.
  apply: rt_step; apply: AE1_Rename; solve_mem.
  by apply: notin_BV_valid_subst; solve_mem.
- by apply: AE_App IH1 IH2.
- by apply: AE_If IH1 IH2 IH3.
Qed.

Lemma a_equiv'_FV : forall t t',
  t ==a t' ->
  FV t =i FV t'.
Proof.
suff H : forall t t', t =a t' -> forall x, x \in FV t -> x \in FV t'.
  move=> t t' /a_equiv_iff aEtt' ?.
  move: (aEtt') => /a_equiv_sym aEt't.
  apply/idP/idP; eauto.
move=> t t' aEtt'.
elim: t t' / aEtt' => [t t' aEtt' | |]; auto.
elim: t t' / aEtt' => [x y T t1 Hy _ | x T t1 t1' _ IH | ???? IH | ???? IH | ????? IH | ????? IH | ????? IH] z Hz;
  try now (case_mem Hz; solve_mem => *; rewrite in_VarsE (IH z); solve_mem).
case_mem Hz => *.
have ? : z != y.
  by apply/eqP => ?; subst; solve_mem.
by rewrite in_VarsE neq_in_FV_subst; solve_mem.
Qed.

Lemma a_equiv'_valid_subst x s s' t :
  valid_subst x s t ->
  s ==a s' ->
  valid_subst x s' t.
Proof.
elim: x s t /; auto.
move=> x s y T t1 Hy1 Hy2 HVS IH aEss'.
apply: VS_Abs_2; auto.
by rewrite -(@a_equiv'_FV s _).
Qed.

(*   The theorem [a_equiv_subst] is our main theorem.                         *)

Theorem a_equiv_subst x t t' s s' :
  t =a t' -> s =a s' ->
  valid_subst x s t ->
  valid_subst x s' t' ->
  {x \-> s} t =a {x \-> s'} t'.
Proof.
rewrite ![_ =a _]a_equiv_iff.
move Heqsi: (size t) => si.
elim/ltn_ind: si t t' Heqsi => ? IH t t' ? aEtt'; subst.
inversion aEtt'; subst.
- rename t' into t.
  case: t {aEtt'} IH => // [y | y T t1 | t1 t2 | t1 t2 t3] IH aEss' HVS HVS'.
  + by simpl; case: (y =P x).
  + inversion HVS; subst.
    * by rewrite /= eq_refl.
    * rewrite /= (negbTE H4).
      apply: AE'_Abs'; apply: (IH (size t1)); auto.
      by apply: (@a_equiv'_valid_subst _ s _ _).
    * by rewrite !notin_FV_subst_xst; solve_mem.
  + inversion HVS; subst.
    inversion HVS'; subst.
    simpl; apply: AE'_App.
      apply: (IH (size t1)); auto.
      by rewrite /= ltnS leq_addr.
    apply: (IH (size t2)); auto.
    by rewrite /= ltnS leq_addl.
  + inversion HVS; subst.
    inversion HVS'; subst.
    simpl; apply: AE'_If.
    * apply: (IH (size t1)); auto.
      by rewrite /= ltnS -addnA leq_addr.
    * apply: (IH (size t2)); auto.
      by rewrite /= ltnS addnAC leq_addl.
    * apply: (IH (size t3)); auto.
      by rewrite /= ltnS leq_addl.
- rename z into w.
  rename y into z.
  rename x0 into y.
  clear w H H0 H1.
  move=> Hss' HVS HVS'.
  inversion HVS; subst.
  + rewrite !notin_FV_subst_xst; solve_mem.
    by rewrite -(@a_equiv'_FV (Abs y T t1)); solve_mem.
  + inversion HVS'; subst.
    * rewrite !notin_FV_subst_xst; solve_mem.
      by rewrite (@a_equiv'_FV _ (Abs z T t1')); solve_mem.
    * rewrite /= (negbTE H4) (negbTE H7).
      have [w Hw] := existence_of_fresh_var (App (Var x) (App (App s s') (App (App (Abs y T t1) (Abs z T t1')) (App (Abs y T ({x \-> s} t1)) (Abs z T ({x \-> s'} t1')))))).
      apply: (@AE'_Abs w); solve_mem.
      rewrite ![{_ \-> _} {x \-> _} _]subst_as_bu_t; solve_mem.
      apply: (IH (size ({y \-> Var w} t1))); auto.
      -- by rewrite size_subst_xyt.
      -- by apply: (a_equiv'_Abs aEtt'); solve_mem.
      -- by apply: valid_subst_subst; solve_mem.
      -- by apply: valid_subst_subst; solve_mem.
    * rewrite !notin_FV_subst_xst; solve_mem.
      by rewrite (@a_equiv'_FV _ (Abs z T t1')); solve_mem.
  + rewrite !notin_FV_subst_xst; solve_mem.
    by rewrite -(@a_equiv'_FV (Abs y T t1)); solve_mem.
- move=> aEss' HVS HVS'.
  inversion HVS; subst.
  inversion HVS'; subst.
  simpl; apply: AE'_App.
    apply: (IH (size t1)); auto.
    by rewrite /= ltnS leq_addr.
  apply: (IH (size t2)); auto.
  by rewrite /= ltnS leq_addl.
- move=> aEss' HVS HVS'.
  inversion HVS; subst.
  inversion HVS'; subst.
  simpl; apply: AE'_If.
  + apply: (IH (size t1)); auto.
    by rewrite /= ltnS -addnA leq_addr.
  + apply: (IH (size t2)); auto.
    by rewrite /= ltnS -addnAC leq_addl.
  + apply: (IH (size t3)); auto.
    by rewrite /= ltnS leq_addl.
Qed.

(*   Next, we prove that all substitutions can be converted to                *)
(*   capture-avoiding one by renaming bound variables in its components.      *)

(*   The function [sep] renumbers bound variables in [t] so that [sep n t]    *)
(* satisfies the following conditions:                                        *)
(* - [sep n t] is a-equivalent to [t].                                        *)
(* - All bound variables in [sep n t] do not have the same indices of the     *)
(*   free variables.                                                          *)
(* - All bound variables in [sep n t] are greater than [n].                   *)
Fixpoint sep n t :=
  match t with
  | Abs x T t1 =>
    let t1' := sep n t1 in
    let x' := (maxn n (maxn (\max_(i <- FV t1') i) (\max_(i <- BV t1') i))).+1 in
    Abs x' T ({x \-> Var x'} t1')
  | App t1 t2 => App (sep n t1) (sep n t2)
  | IfExp t1 t2 t3 => IfExp (sep n t1) (sep n t2) (sep n t3)
  | _ => t
  end.

Lemma valid_subst_sep n x s t :
  n >= \max_(i <- FV s) i ->
  valid_subst x s (sep n t).
Proof.
move=> n_ge_mx.
apply: in_BV_notin_FV_valid_subst.
elim: t => //= [y T t1 IH | ???? | ??????] z z_in_BV; try now (case_mem z_in_BV; intuition).
case_mem z_in_BV.
- move/eqP->.
  apply/memPn => w w_in_FV.
  have w_le_n : w <= n.
    by apply: (leq_trans _ n_ge_mx); apply: leq_bigmax_seq.
  set mx := maxn _ _.  
  have n_le_mx : n <= mx.
    by apply: leq_maxl.
  have w_le_mx := leq_trans w_le_n n_le_mx.
  by rewrite neq_ltn; apply/orP; left; rewrite ltnS.
- rewrite in_BV_subst; apply: IH.
Qed.

Lemma a_equiv_sep n t :
    t =a sep n t.
Proof.
elim: t => /= [x | x T t1 IH | t1 IH1 t2 IH2 | | | t1 IH1 t2 IH2 t3 IH3]; try apply: rt_refl.
- apply: rt_trans.
    by apply: AE_Abs IH.
  apply: rt_step; apply: AE1_Rename.
    apply/memPn => y y_in_FV.
    rewrite neq_ltn; apply/orP; left.
    by rewrite ltnS (leq_trans (leq_bigmax_seq y_in_FV)) // maxnCA leq_maxl.
  apply: notin_BV_valid_subst.
  apply/memPn => y y_in_BV.
  rewrite neq_ltn; apply/orP; left.
  by rewrite ltnS (leq_trans (leq_bigmax_seq y_in_BV)) // maxnA leq_maxr.
- by apply: AE_App.
- by apply: AE_If.
Qed.

Definition avoid_capture x s t :=
  if valid_substp x s t then t else sep (\max_(i <- FV s) i) t.

Theorem a_equiv_avoid_capture x s t :
  t =a avoid_capture x s t.
Proof.
rewrite /avoid_capture.
case: ifP => _.
  by apply: rt_refl.
by apply: a_equiv_sep.
Qed.

Theorem valid_subst_avoid_capture x s t :
  valid_subst x s (avoid_capture x s t).
Proof. 
rewrite /avoid_capture.
case: (valid_substP x s t) => // _.
by apply: valid_subst_sep.
Qed.

Lemma avoid_capture_id x s t :
  valid_subst x s t ->
  avoid_capture x s t = t.
Proof. by rewrite /avoid_capture; move/valid_substP->. Qed.

(*   Finally, we define capture-avoiding substitution.                        *)

Definition capture_avoiding_subst x s t := nosimpl
   subst x s (avoid_capture x s t).

Notation "'[' x '\->' s ']' t" := (capture_avoiding_subst x s t) (at level 50).

Lemma capture_avoiding_substE x s t :
  valid_subst x s t ->
  [x \-> s] t = {x \-> s} t.
Proof. by move=> ?; rewrite /capture_avoiding_subst avoid_capture_id. Qed.

(*   Appendix: Another proof of [a_equiv_subst] without [==a]                 *)

(*   We allow [auto] tactic to use the constructors of [clos_refl_trans].     *)
(* (The standard library adds the constructors to the hint database [sets]    *)
(*  but not [core] so that, by default, [auto] tactic cannot use them.)       *)

#[local]
Hint Constructors clos_refl_trans : core.

(*   Preparation                                                              *)

Lemma AE_Var_inv x t' :
  Var x =a t' ->
  t' = Var x.
Proof.
move: {1 3}(Var x) (erefl (Var x)) => t Et Ett'.
elim: t t' / Ett' Et => //; first by move=> ??; inversion 1.
by move=> t t'' t' _ IH1 _ IH2 Et; apply/IH2/IH1.
Qed.

Lemma AE_Abs_inv x T t1 t' :
  Abs x T t1 =a t' ->
  exists y t1', t' = Abs y T t1'.
Proof.
move: {1 3}(Abs x T t1) (erefl (Abs x T t1)) => t Et Ett'.
elim: t t' / Ett' x T t1 Et.
- move=> t t'.
  case: t t' / => //.
    move=> x y T t1 _ _ ??? [_ <- _].
    by exists y, ({x \-> Var y} t1).
  move=> x T t1 t1' _ ??? [_ <- _].
  by exists x, t1'.
- move=> ? x T t1 ->.
  by exists x, t1.
- move=> t t'' t' _ IH1 _ IH2 x T t1 Et.
  have [z [t1'' Et'']] := IH1 _ _ _ Et.
  have [y [t1' ->]] := IH2 _ _ _ Et''.
  by exists y, t1'.
Qed.

Lemma AE_App_inv t1 t2 t' :
  App t1 t2 =a t' ->
  exists t1' t2', t' = App t1' t2' /\
                  t1 =a t1' /\
                  t2 =a t2'.
Proof.
move: {1 3}(App t1 t2) (erefl (App t1 t2)) => t Et Ett'.
elim: t t' / Ett' t1 t2 Et.
- move=> t t' Ett' t1 t2 ?; subst.
  inversion Ett'; subst.
  + by exists t1', t2; auto.
  + by exists t1, t2'; auto.
- by move=> t t1 t2 ?; exists t1, t2; auto.
- move=> t t'' t' _ IH1 _ IH2 t1 t2 Et.
  have [t1'' [t2'' [Et'' [aE1 aE2]]]] := IH1 _ _ Et.
  have [t1' [t2' [Et' [aE1' aE2']]]] := IH2 _ _ Et''.
  by exists t1', t2'; do !split => //; eauto.
Qed.

Lemma AE_Tru_inv t' :
  Tru =a t' ->
  t' = Tru.
Proof.
move: {1 3}Tru (erefl Tru) => t Et Ett'.
elim: t t' / Ett' Et => //; first by move=> ??; inversion 1.
by move=> t t'' t' _ IH1 _ IH2 Et; apply/IH2/IH1.
Qed.

Lemma AE_Fal_inv t' :
  Fal =a t' ->
  t' = Fal.
Proof.
move: {1 3}Fal (erefl Fal) => t Et Ett'.
elim: t t' / Ett' Et => //; first by move=> ??; inversion 1.
by move=> t t'' t' _ IH1 _ IH2 Et; apply/IH2/IH1.
Qed.

Lemma AE_If_inv t1 t2 t3 t' :
  IfExp t1 t2 t3 =a t' ->
  exists t1' t2' t3', t' = IfExp t1' t2' t3' /\
                      t1 =a t1' /\
                      t2 =a t2' /\
                      t3 =a t3'.
Proof.
move: {1 3}(IfExp t1 t2 t3) (erefl (IfExp t1 t2 t3)) => t Et Ett'.
elim: t t' / Ett' t1 t2 t3 Et.
- move=> t t' Ett' t1 t2 t3 ?; subst.
  inversion Ett'; subst.
  + by exists t1', t2, t3; do !split => //; auto.
  + by exists t1, t2', t3; do !split => //; auto.
  + by exists t1, t2, t3'; do !split => //; auto.
- by move=> t t1 t2 t3 ?; exists t1, t2, t3; auto.
- move=> t t'' t' _ IH1 _ IH2 t1 t2 t3 Et.
  have [t1'' [t2'' [t3'' [Et'' [aE1 [aE2 aE3]]]]]] := IH1 _ _ _ Et.
  have [t1' [t2' [t3' [Et' [aE1' [aE2' aE3']]]]]] := IH2 _ _ _ Et''.
  by exists t1', t2', t3'; do !split => //; eauto.
Qed.

(*   The lemma [a_equiv_FV] is a counterpart of [a_equiv'_FV].                *)

Lemma a_equiv_FV t t' :
  t =a t' ->
  FV t =i FV t'.
Proof.
elim: t t' / => // [t t' | t t'' t']; last first.
  by move=> _ ? _ ? ?; eauto.
elim: t t' / => [x y T t1 y_notin_FV _ | x T t1 t1' _ E | t1 t1' t2 _ E | t1 t2 t2' _ E | t1 t1' t2 t3 _ E | t1 t2 t2' t3 _ E | t1 t2 t3 t3' _ E] z.
- rewrite !in_FV_Abs.
  apply/andP/andP.
    case => z_neq_x z_in_FV.
    have z_neq_y: z != y.
      by move: y_notin_FV; apply: contraNneq => <-.
    by rewrite neq_in_FV_subst.
  case: (x =P y) => [-> | /eqP x_neq_y]; first by rewrite subst_xxt.
  case => z_neq_y z_in_FV.
  have z_neq_x: z != x.
    have := neq_notin_FV_subst t1 x_neq_y.
    by apply: contraNneq => {1}<-.
  by rewrite neq_in_FV_subst in z_in_FV.
- by rewrite !in_FV_Abs E.
- by rewrite !in_FV_App E.
- by rewrite !in_FV_App E.
- by rewrite !in_FV_If E.
- by rewrite !in_FV_If E.
- by rewrite !in_FV_If E.
Qed.

(*   The theorem [a_equiv_Abs] is a counterpart of [a_equiv'_Abs].            *)

Theorem a_equiv_Abs x T t1 y t1' :
  Abs x T t1 =a Abs y T t1' ->
  exists X : seq nat, forall z,
      z \notin X ->
      {x \-> Var z} t1 =a {y \-> Var z} t1'.
Proof.
move: {1 3}(Abs y T t1') (erefl (Abs y T t1')) => t' Et'.
move: {1 3}(Abs x T t1) (erefl (Abs x T t1)) => t Et Ett'.
elim: t t' / Ett' x T t1 y t1' Et Et'.
- move=> t t'.
  case: t t' / => //.
    move=> x y T t1 y_notin_FV_t1 vs_xyt1 ????? [<- <- <-] [<- <-].
    exists (Vars (App (Abs x T t1) (Abs y T ({x \-> Var y} t1)))) => z z_notin_Vars.
    by rewrite subst_yz_xy_t; eauto.
  move=> x T t1 t1' Et1t1' ????? [<- <- <-] [<- <-].
  exists (Vars (App (Abs x T t1) (Abs x T t1'))) => z z_notin_Vars.
  elim: t1 t1' / Et1t1' z_notin_Vars.
  + move=> u v T' t11 v_notin_FV vs_uvt11 z_notin_Vars.
    have Et1t1' : Abs u T' t11 =a Abs v T' ({u \-> Var v} t11) by apply/rt_step/AE1_Rename.
    have := a_equiv_FV Et1t1'.
    case: (u =P x) => [<- /(_ u) | /eqP u_neq_x].
      rewrite in_FV_Abs eq_refl => /esym/negbT notin_u.
      rewrite [{_ \-> _} Abs u _ _]/= eq_refl.
      by rewrite notin_FV_subst_xst.
    case: (v =P x) => [<- /(_ v) | /eqP v_neq_x].
      rewrite [_ \in FV (Abs v _ _)]in_FV_Abs eq_refl => /negbT notin_v.
      rewrite notin_FV_subst_xst //.
      by rewrite /= eq_refl.
    move=> _.
    rewrite /= (negbTE u_neq_x) (negbTE v_neq_x).
    rewrite subst_ab_cd_t; solve_mem.
    apply/rt_step/AE1_Rename; first by rewrite neq_in_FV_subst; solve_mem.
    by apply: valid_subst_subst => //; solve_mem.
  + move=> w T' t11 t11' Et11t11' IH z_notin_Vars /=.
    case: eqP => _; apply: AE_Abs; first by apply: rt_step.
    by apply: IH; solve_mem.
  + move=> t11 t11' t12 _ IH z_notin_Vars /=.
    apply: AE_App; last by exact: rt_refl.
    by apply: IH; solve_mem.
  + move=> t11 t12 t12' _ IH z_notin_Vars /=.
    apply: AE_App; first by exact: rt_refl.
    by apply: IH; solve_mem.
  + move=> t11 t11' t12 t13 _ IH z_notin_Vars /=.
    apply: AE_If; try exact: rt_refl.
    by apply: IH; solve_mem.
  + move=> t11 t12 t12' t13 _ IH z_notin_Vars /=.
    apply: AE_If; try exact: rt_refl.
    by apply: IH; solve_mem.
  + move=> t11 t12 t13 t13' _ IH z_notin_Vars /=.
    apply: AE_If; try exact: rt_refl.
    by apply: IH; solve_mem.
- move=> t x T t1 y t1' -> -[] <- <-.
  by exists [::]; eauto.
- move=> t t'' t' + + _ + x T t1 y t1' Et Et'.
  rewrite {}Et {}Et'.
  move=> /AE_Abs_inv [z [t1'' ->]].
  move/(_ _ _ _ _ _ erefl erefl) => [X HX].
  move/(_ _ _ _ _ _ erefl erefl) => [Y HY].
  exists (X ++ Y) => w.
  rewrite mem_cat; case/norP => w_notin_X w_notin_Y.
  have aE1 := HX _ w_notin_X.
  have aE2 := HY _ w_notin_Y.
  by eauto.
Qed.

(*   The lemma [notin_FV_notin_FV_subst_s] is a strong version of             *)
(* [notin_FV_notin_FV_subst].                                                 *)

Lemma notin_FV_notin_FV_subst_s x y s t :
  x \notin FV t ->
  x \notin FV s ->
  x \notin FV ({y \-> s} t).
Proof.
elim: t => //= [z | z T t1 IH | t1 IH1 t2 IH2 | t1 IH1 t2 IH2 t3 IH3].
- by move=> *; case: (z =P y).
- case_mem => *.
    by case: (z =P y); solve_mem.
  by case: (z =P y); solve_mem.
- by case_mem => *; solve_mem.
- by case_mem => *; solve_mem.
Qed.

(*   The lemma [notin_Vars_notin_Vars_subst_s] is a strong version of         *)
(* [notin_Vars_notin_Vars_subst].                                             *)

Lemma notin_Vars_notin_Vars_subst_s x y s t :
  x \notin Vars t ->
  x \notin Vars s ->
  x \notin Vars ({y \-> s} t).
Proof.
elim: t => //= [z | z T t1 IH | t1 IH1 t2 IH2 | t1 IH1 t2 IH2 t3 IH3].
- case: (z =P y) => // _; solve_mem.
- by case: (z =P y) => // _; solve_mem.
- case_mem => x_notin_Vars1 x_notin_Vars2 x_notin_s.
  by rewrite in_Vars_App negb_or IH1 // IH2.
- case_mem => x_notin_Vars1 x_notin_Vars2 x_notin_Vars3 x_notin_s.
  by rewrite in_Vars_If !negb_or IH1 // IH2 // IH3.
Qed.

Theorem a_equiv_subst' x t t' s s' :
  t =a t' -> s =a s' ->
  valid_subst x s t ->
  valid_subst x s' t' ->
  {x \-> s} t =a {x \-> s'} t'.
Proof.
move Heqsi: (size t) => si.
elim/ltn_ind: si t t' Heqsi => ? + t t' ? +; subst.
case: t.
- move=> y _ /AE_Var_inv -> aEss' _ _ /=.
  by case: eqP => // _; auto.
- move=> y T t1 IH /[dup]/AE_Abs_inv [z [t1' ->]].
  move=> /[dup]/a_equiv_FV => EFV aEtt' Ess' HVS HVS'.
  inversion HVS; subst.
  + rewrite !notin_FV_subst_xst; solve_mem.
    by rewrite -EFV; solve_mem.
  + inversion HVS'; subst.
    * rewrite !notin_FV_subst_xst; solve_mem.
      by rewrite EFV; solve_mem.
    * rewrite /= (negbTE H4) (negbTE H7).
      have [X HX] := a_equiv_Abs aEtt'.
      have [w] := existence_of_nat_notin_seq (X ++ Vars (App t1 (App t1' (App (Var x) (App s s'))))).
      rewrite mem_cat => /norP [w_notin_X w_notin_Vars].
      have aE := HX _ w_notin_X.
      have aE1 : Abs y T ({x \-> s} t1) =a Abs w T ({y \-> Var w} {x \-> s} t1).
        apply/rt_step/AE1_Rename; first by apply: notin_FV_notin_FV_subst_s; solve_mem.
        apply: notin_BV_valid_subst.
        have : w \notin Vars ({x \-> s} t1) by apply: notin_Vars_notin_Vars_subst_s; solve_mem.
        by solve_mem.
      have aE2 : Abs w T ({y \-> Var w} {x \-> s} t1) =a Abs w T ({z \-> Var w} {x \-> s'} t1').
        apply: AE_Abs.
        rewrite ![{_ \-> Var _} {_ \-> _} _]subst_as_bu_t; solve_mem.
        apply: (IH (size ({y \-> Var w} t1))) => //.
        ++ by rewrite size_subst_xyt.
        ++ by apply: valid_subst_subst; solve_mem.
        ++ by apply: valid_subst_subst; solve_mem.
      have aE3 : Abs w T ({z \-> Var w} {x \-> s'} t1') =a Abs z T ({x \-> s'} t1').
        apply: a_equiv_sym.
        apply/rt_step/AE1_Rename; first by apply: notin_FV_notin_FV_subst_s; solve_mem.
        apply: notin_BV_valid_subst.
        have : w \notin Vars ({x \-> s'} t1') by apply: notin_Vars_notin_Vars_subst_s; solve_mem.
        by solve_mem.
      by eauto.
    * rewrite !notin_FV_subst_xst; solve_mem.
      by rewrite EFV; solve_mem.
  + rewrite !notin_FV_subst_xst; solve_mem.
    by rewrite -EFV; solve_mem.
- move=> t1 t2 IH /AE_App_inv [t1' [t2' [-> [??]]]] aEss' HVS HVS' /=.
  inversion HVS; subst.
  inversion HVS'; subst.
  apply: AE_App.
  + by apply: (IH (size t1)) => //; rewrite /= ltnS leq_addr.
  + by apply: (IH (size t2)) => //; rewrite /= ltnS leq_addl.
- by move=> _ /AE_Tru_inv -> _ _ _; auto.
- by move=> _ /AE_Fal_inv -> _ _ _; auto.
- move=> t1 t2 t3 IH /AE_If_inv [t1' [t2' [t3' [-> [? [??]]]]]] aEss' HVS HVS' /=.
  inversion HVS; subst.
  inversion HVS'; subst.
  apply: AE_If.
  + by apply: (IH (size t1)) => //; rewrite /= ltnS -addnA leq_addr.
  + by apply: (IH (size t2)) => //; rewrite /= ltnS addnAC leq_addl.
  + by apply: (IH (size t3)) => //; rewrite /= ltnS leq_addl.
Qed.
