From mathcomp Require Import all_ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Lemma leq_bigmax_seq x s (x_in_s : x \in s) :
  x <= \max_(y <- s) y.
Proof. by rewrite (big_rem x x_in_s) leq_maxl. Qed.
