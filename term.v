From mathcomp Require Import all_ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

From LC Require Import bigop_ext.

(* size t: the count of nodes in t                                            *)
(*   FV t: the set of free variables in t                                     *)
(*   BV t: the set of bound variables in t                                    *)
(* Vars t: the set of variables in t                                          *)

Inductive ty :=
| TyBool
| TyFun of ty & ty. (* T -> T *)

Inductive term :=
| Var of nat
| Abs of nat & ty & term (* \x:T. t *)
| App of term & term
| Tru
| Fal
| IfExp of term & term & term. (* if t then t else t *)

Inductive is_value : term -> Prop :=
| V_Abs : forall x T t1, is_value (Abs x T t1)
| V_Tru : is_value Tru
| V_Fal : is_value Fal.

#[export]
Hint Constructors is_value : core.

Fixpoint size t :=
  match t with
    Var _ => 1
  | Abs _ _ t1 => (size t1).+1
  | App t1 t2 => (size t1 + size t2).+1
  | Tru => 1
  | Fal => 1
  | IfExp t1 t2 t3 => (size t1 + size t2 + size t3).+1
  end.

Fixpoint FV_rec t :=
  match t with
    Var x => [:: x]
  | Abs x _ t1 => filter (predC1 x) (FV_rec t1)
  | App t1 t2 => FV_rec t1 ++ FV_rec t2
  | IfExp t1 t2 t3 => FV_rec t1 ++ FV_rec t2 ++ FV_rec t3
  | _ => [::]
  end.
Definition FV t := nosimpl FV_rec t.

Lemma in_FV_Var x y :
  (x \in FV (Var y)) = (x == y).
Proof. by rewrite /FV /= mem_seq1. Qed.

Lemma in_FV_Abs x y T t1 :
  (x \in FV (Abs y T t1)) =
  (x != y) && (x \in FV t1).
Proof. by rewrite /FV /= mem_filter. Qed.

Lemma notin_FV_Abs x y T t1 :
  (x \notin FV (Abs y T t1)) =
  (x == y) || ((x != y) && (x \notin FV t1)).
Proof. 
  rewrite /FV /= (in_FV_Abs _ _ T _) negb_and.
  by case: (x =P y).
Qed.

Lemma in_FV_App x t1 t2 :
  (x \in FV (App t1 t2)) =
  (x \in FV t1) || (x \in FV t2).
Proof. by rewrite /FV /= mem_cat. Qed.

Lemma in_FV_Tru x :
  (x \in FV Tru) = false.
Proof. by []. Qed.

Lemma in_FV_Fal x :
  (x \in FV Fal) = false.
Proof. by []. Qed.

Lemma in_FV_If x t1 t2 t3 :
  (x \in FV (IfExp t1 t2 t3)) =
  (x \in FV t1) || (x \in FV t2) || (x \in FV t3).
Proof. by rewrite /FV /= !mem_cat orbA. Qed.

Fixpoint BV_rec t :=
  match t with
    Var _ => [::]
  | Abs x _ t1 => x :: BV_rec t1
  | App t1 t2 => BV_rec t1 ++ BV_rec t2
  | IfExp t1 t2 t3 => BV_rec t1 ++ BV_rec t2 ++ BV_rec t3
  | _ => [::]
  end.
Definition BV t := nosimpl BV_rec t.

Lemma in_BV_Var x y :
  (x \in BV (Var y)) = false.
Proof. by rewrite /BV /= in_nil. Qed.

Lemma in_BV_Abs x y T t1 :
  (x \in BV (Abs y T t1)) =
  (x == y) || (x \in BV t1).
Proof. by rewrite /BV /= in_cons. Qed.

Lemma in_BV_App x t1 t2 :
  (x \in BV (App t1 t2)) =
  (x \in BV t1) || (x \in BV t2).
Proof. by rewrite /BV /= mem_cat. Qed.

Lemma in_BV_Tru x :
  (x \in BV Tru) = false.
Proof. by []. Qed.

Lemma in_BV_Fal x :
  (x \in BV Fal) = false.
Proof. by []. Qed.

Lemma in_BV_If x t1 t2 t3 :
  (x \in BV (IfExp t1 t2 t3)) =
  (x \in BV t1) || (x \in BV t2) || (x \in BV t3).
Proof. by rewrite /BV /= !mem_cat orbA. Qed.

Fixpoint Vars_rec t :=
  match t with
    Var x => [:: x]
  | Abs x _ t1 => (x :: Vars_rec t1)
  | App t1 t2 => (Vars_rec t1 ++ Vars_rec t2)
  | IfExp t1 t2 t3 => Vars_rec t1 ++ Vars_rec t2 ++ Vars_rec t3
  | _ => [::]
  end.
Definition Vars t := nosimpl Vars_rec t.

Lemma in_Vars_Var x y :
  (x \in Vars (Var y)) = (x == y).
Proof. by rewrite /Vars /= mem_seq1. Qed.

Lemma in_Vars_Abs x y T t1 :
  (x \in Vars (Abs y T t1)) =
  (x == y) || (x \in Vars t1).
Proof. by rewrite /Vars /= in_cons. Qed.

Lemma in_Vars_App x t1 t2 :
  (x \in Vars (App t1 t2)) =
  (x \in Vars t1) || (x \in Vars t2).
Proof. by rewrite /Vars /= mem_cat. Qed.

Lemma in_Vars_Tru x :
  (x \in Vars Tru) = false.
Proof. by []. Qed.

Lemma in_Vars_Fal x :
  (x \in Vars Fal) = false.
Proof. by []. Qed.

Lemma in_Vars_If x t1 t2 t3 :
  (x \in Vars (IfExp t1 t2 t3)) =
  (x \in Vars t1) || (x \in Vars t2) || (x \in Vars t3).
Proof. by rewrite /Vars /= !mem_cat orbA. Qed.

Lemma in_Vars_FV_V_BV x t :
  (x \in Vars t) =
  (x \in FV t) || (x \in BV t).
Proof.
  elim: t => [y | y t1 T IH | t1 IH1 t2 IH2 | | | t1 IH1 t2 IH2 t3 IH3] //.
  - rewrite in_BV_Var orbF //.
  - rewrite in_Vars_Abs in_FV_Abs in_BV_Abs.
    by case: (x =P y).
  - rewrite in_Vars_App in_FV_App in_BV_App.
    rewrite orbACA IH1 IH2 //.
  - rewrite !in_Vars_If !in_FV_If !in_BV_If.
    rewrite orbACA.
    congr (_ || _); auto.
    rewrite orbACA.
    congr (_ || _); auto.
Qed.

Lemma existence_of_nat_notin_seq (s : seq nat) :
  exists n, n \notin s.
Proof.
  exists (\max_(m <- s) m).+1.
  apply/memPn => m m_in_s.
  rewrite neq_ltn; apply/orP; left.
  by rewrite ltnS leq_bigmax_seq.
Qed.

Theorem existence_of_fresh_var t :
  exists x, x \notin Vars t.
Proof.
  exact: existence_of_nat_notin_seq.
Qed.
