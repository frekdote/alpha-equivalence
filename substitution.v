From mathcomp Require Import all_ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

From LC Require Import term tactics.

(*        {x \-> s} t: the term obtained by replacing all free occurrences of *)
(*                     x in t with s                                          *)
(*  valid_subst x s t: {x \-> s} t is capture-avoiding                        *)
(* valid_substp x s t: whether {x \-> s} t is capture-avoiding                *)
(*                                                                            *)
(*   Note that {x \-> s} t may cause variable capture.                        *)

Fixpoint subst x s t :=
  match t with
    Var y => if y == x then s else Var y
  | Abs y T t1 => if y == x then Abs y T t1 else Abs y T (subst x s t1)
  | App t1 t2 => App (subst x s t1) (subst x s t2)
  | IfExp t1 t2 t3 => IfExp (subst x s t1) (subst x s t2) (subst x s t3)
  | _ => t
  end.

Notation "'{' x '\->' s '}' t" := (subst x s t) (at level 50, t at level 60, right associativity).

Inductive valid_subst : nat -> term -> term -> Prop :=
  VS_Var : forall x s y, valid_subst x s (Var y)
| VS_Abs_1 : forall x s T t1, valid_subst x s (Abs x T t1)
| VS_Abs_2 : forall x s y T t1, y != x -> y \notin FV s -> valid_subst x s t1 -> valid_subst x s (Abs y T t1)
| VS_Abs_3 : forall x s y T t1, y != x -> x \notin FV t1 -> valid_subst x s t1 -> valid_subst x s (Abs y T t1)
| VS_App : forall x s t1 t2, valid_subst x s t1 -> valid_subst x s t2 -> valid_subst x s (App t1 t2)
| VS_Tru : forall x s, valid_subst x s Tru
| VS_Fal : forall x s, valid_subst x s Fal
| VS_If : forall x s t1 t2 t3, valid_subst x s t1 -> valid_subst x s t2 -> valid_subst x s t3 -> valid_subst x s (IfExp t1 t2 t3).

#[export]
Hint Constructors valid_subst : core.

Fixpoint valid_substp x s t :=
  match t with
    Var _ => true
  | App t1 t2 => valid_substp x s t1 && valid_substp x s t2
  | Abs y _ t1 => (y == x) || ((y != x) && (y \notin FV s) && valid_substp x s t1) || ((y != x) && (x \notin FV t1) && valid_substp x s t1)
  | Tru => true
  | Fal => true
  | IfExp t1 t2 t3 => valid_substp x s t1 && valid_substp x s t2 && valid_substp x s t3
  end.

Lemma valid_substP x s t :
  reflect (valid_subst x s t) (valid_substp x s t).
Proof.
apply/(iffP idP).
  elim: t => //= [y T t1 IH | ???? | ??????].
  + rewrite -orbA; case/orP.
      by move/eqP->.
    by case/orP; do ! case/andP; auto.
  + by case/andP; auto.
  + by do ! case/andP; auto.
by elim: x s t / => * /=;
   do ! match goal with
        | H : is_true ?x |- context [?x] => rewrite H //=
        | |- context [?x == ?x] => rewrite eq_refl //=
        | _ => rewrite ?orbT //=
        end.
Qed.

Section SizeSubst.
Lemma size_subst_xyt x y t :
  size ({x \-> Var y} t) = size t.
Proof.
elim: t => //= [z | z T t1 IH | t1 IH1 t2 IH2 | t1 IH1 t2 IH2 t3 IH3].
- by case: ifP.
- by case: ifP => _; congr _.+1.
- by congr (_ + _).+1.
- by congr (_ + _ + _).+1.
Qed.
End SizeSubst.

Section VarsSubst.
Lemma neq_in_FV_subst x y z t :
  z != x -> z != y ->
  (z \in FV ({x \-> Var y} t)) = (z \in FV t).
Proof.
move=> z_neq_x z_neq_y.
elim: t => //= [w | w T t1 IH | t1 IH1 t2 IH2 | t1 IH1 t2 IH2 t3 IH3].
- by case: (w =P x) => // ->; solve_mem.
- by case: (w =P x) => // _; rewrite !in_FV_Abs; congr (_ && _).
- by rewrite !in_FV_App IH1 IH2.
- by rewrite !in_FV_If IH1 IH2 IH3.
Qed.

Lemma neq_notin_FV_subst x y t :
  x != y ->
  x \notin FV ({x \-> Var y} t).
Proof.
move=> x_neq_y.
elim: t => //= [z | z T t1 IH | |]; solve_mem.
  by case: (z =P x) => /eqP; solve_mem.
by case: (z =P x) => /eqP; solve_mem.
Qed.

Lemma notin_FV_notin_FV_subst x y z t :
  x \notin FV t ->
  x != z ->
  x \notin FV ({y \-> Var z} t).
Proof.
elim: t => //= [w | w T t1 IH | t1 IH1 t2 IH2 | t1 IH1 t2 IH2 t3 IH3].
- by move=> *; case: (w =P y) => //; solve_mem.
- case_mem => *.
    by case: (w =P y); solve_mem.
  by case: (w =P y); solve_mem.
- by case_mem => *; solve_mem.
- by case_mem => *; solve_mem.
Qed.

Lemma in_BV_subst x y z t :
  (z \in BV ({x \-> Var y} t)) = (z \in BV t).
Proof.
elim: t => //= [w | w T t1 IH | t1 IH1 t2 IH2 | t1 IH1 t2 IH2 t3 IH3].
- by case: (w =P x) => ?; subst.
- case: ifP => // ?.
  by rewrite !in_BV_Abs; congr (_ || _).
- by rewrite !in_BV_App; congr (_ || _).
- by rewrite !in_BV_If; congr (_ || _ || _).
Qed.

Lemma notin_Vars_notin_Vars_subst x y z t :
  x \notin Vars t ->
  x != z ->
  x \notin Vars ({y \-> Var z} t).
Proof.
elim: t => //= [w | w T t1 IH | t1 IH1 t2 IH2 | t1 IH1 t2 IH2 t3 IH3].
- by case: (w =P y) => // _; solve_mem.
- by case: (w =P y) => // _; solve_mem.
- case_mem => x_notin_Vars1 x_notin_Vars2 z_neq_x.
  by rewrite in_Vars_App negb_or IH1 // IH2.
- case_mem => x_notin_Vars1 x_notin_Vars2 x_notin_Vars3 z_neq_x.
  by rewrite in_Vars_If !negb_or IH1 // IH2 // IH3.
Qed.
End VarsSubst.

Section Subst.
Lemma subst_xxt x t :
  {x \-> Var x} t = t.
Proof.
elim: t => //= [y | y T t1 IH | t1 IH1 t2 IH2 | t1 IH1 t2 IH2 t3 IH3].
- by case: (y =P x) => // ->.
- by case: (y =P x) => // _; congr Abs.
- by rewrite IH1 IH2.
- by rewrite IH1 IH2 IH3 //.
Qed.

Lemma valid_subst_xxt x t :
  valid_subst x (Var x) t.
Proof.
elim: t => // [y T t1 IH | |]; auto.
by case: (y =P x) => [-> // | /eqP ?]; apply: VS_Abs_2; solve_mem.
Qed.

Lemma notin_FV_subst_xst x s t :
  x \notin FV t -> {x \-> s} t = t.
Proof.
elim: t => //= [y | y T t1 IH | t1 IH1 t2 IH2 | t1 IH1 t2 IH2 t3 IH3].
- by case_mem => /negbTE x_neq_y; rewrite eq_sym x_neq_y.
- case_mem.
    by rewrite eq_sym => ->.
  move=> /negbTE x_neq_y x_notin_FV.
  by rewrite eq_sym x_neq_y; congr Abs; apply: IH.
- by case_mem => *; rewrite IH1 // IH2.
- by case_mem => *; rewrite IH1 // IH2 // IH3.
Qed.

Lemma notin_FV_valid_subst_xst x s t :
  x \notin FV t -> valid_subst x s t.
Proof.
elim: t => // [y T t1 IH | ???? | ??????].
- case_mem => [/eqP -> // | ??].
  by apply: VS_Abs_3; solve_mem.
- by case_mem; auto.
- by case_mem; auto.
Qed.

Lemma subst_yz_xy_t x y z t :
  y \notin FV t ->
  valid_subst x (Var y) t ->
  {y \-> Var z} {x \-> Var y} t = {x \-> Var z} t.
Proof.
elim: t => // [w | w T t1 IH | t1 IH1 t2 IH2 | t1 IH1 t2 IH2 t3 IH3] y_notin_FV VS.
- by case: (w =P x) => [-> | /eqP]; solve_mem.
- inversion VS; subst.
  + case_mem y_notin_FV => [/eqP -> | /negbTE y_neq_w ?]; solve_mem.
    rewrite /= eq_refl /= eq_sym y_neq_w.
    by rewrite notin_FV_subst_xst.
  + case_mem H5 => H5.
    rewrite /= (negbTE H4) /= (negbTE H5).
    by congr Abs; apply: IH; solve_mem.
  + rewrite ![{x \-> _} _]notin_FV_subst_xst; solve_mem.
    case_mem y_notin_FV => [/eqP -> | /negbTE y_neq_w ?]; solve_mem.
    rewrite /= eq_sym y_neq_w.
    by rewrite notin_FV_subst_xst.
- case_mem y_notin_FV => *.
  inversion VS; subst.
  by rewrite /= IH1 // IH2.
- case_mem y_notin_FV => *.
  inversion VS; subst.
  by rewrite /= IH1 // IH2 // IH3.
Qed.

Lemma subst_yx_xy_t x y t :
  y \notin FV t ->
  valid_subst x (Var y) t ->
  {y \-> Var x} {x \-> Var y} t = t.
Proof. by move=> *; rewrite subst_yz_xy_t // subst_xxt. Qed.

Lemma valid_subst_yx_xy_t x y t :
  y \notin FV t ->
  valid_subst x (Var y) t -> 
  valid_subst y (Var x) ({x \-> Var y} t).
Proof.
elim: t => [z | z T t1 IH | ???? | | | ??????] Hy HVS /=; try now (simpl; case_mem Hy; inversion HVS; auto).
  by simpl; case: (z =P x).
inversion HVS; subst.
- rewrite eq_refl.
  case_mem Hy => [/eqP -> // | *].
  by apply: notin_FV_valid_subst_xst; solve_mem.
- rewrite (negbTE H4).
  by apply: VS_Abs_2; solve_mem.
- rewrite (negbTE H4) notin_FV_subst_xst //.
  case_mem Hy => [/eqP -> // | Hy1 Hy2].
  apply: VS_Abs_3; solve_mem.
  by apply: notin_FV_valid_subst_xst.
Qed.

Lemma subst_xy_xz_t x y z t :
  z != x ->
  {x \-> Var y} {x \-> Var z} t = {x \-> Var z} t.
Proof.
move=> z_neq_x.
elim: t => //= [w | w T t1 IH | t1 IH1 t2 IH2 | t1 IH1 t2 IH2 t3 IH3].
- by case: (w =P x) => /eqP ? /=; solve_mem.
- case: (w =P x) => /= [-> | /eqP/negbTE ->].
    by rewrite eq_refl.
  by congr Abs.
- by congr App.
- by congr IfExp.
Qed.

Lemma subst_as_bu_t a s b u t :
  a != b ->
  a \notin FV u ->
  b \notin FV s ->
  {a \-> s} {b \-> u} t = {b \-> u} {a \-> s} t.
Proof.
move=> /negbTE a_neq_b a_notin_FV b_notin_FV.
elim: t => //= [x | x T t1 IH | t1 IH1 t2 IH2 | t1 IH1 t2 IH2 t3 IH3].
- case: (x =P a) => [-> | /eqP/negbTE x_neq_a].
    by rewrite a_neq_b /= eq_refl notin_FV_subst_xst.
  case: (x =P b) => [-> | /eqP/negbTE x_neq_b].
    by rewrite notin_FV_subst_xst //= eq_refl.
  by rewrite /= x_neq_a x_neq_b.
- case: (x =P a) => [-> | /eqP/negbTE x_neq_a].
    by rewrite /= a_neq_b /= eq_refl.
  case: (x =P b) => [-> | /eqP/negbTE x_neq_b].
    by rewrite /= eq_sym a_neq_b eq_refl.
  by rewrite /= x_neq_a x_neq_b; congr Abs.
- by congr App.
- by congr IfExp.
Qed.

Lemma subst_ab_cd_t a b c d t :
  a != c ->
  a != d ->
  b != c ->
  {a \-> Var b} {c \-> Var d} t = {c \-> Var d} {a \-> Var b} t.
Proof. by move=> *; apply: subst_as_bu_t; solve_mem. Qed.

Lemma notin_BV_valid_subst x y t :
  y \notin BV t -> valid_subst x (Var y) t.
Proof.
elim: t => // [z T t1 IH1 | ???? | ??????] Hy; try now (case_mem Hy; auto).
case: (z =P x) => [-> // | /eqP Hzx].
case_mem Hy => *.
by apply: VS_Abs_2; solve_mem.
Qed.

Lemma in_BV_notin_FV_valid_subst x s t :
  (forall x, x \in BV t -> x \notin FV s) ->
  valid_subst x s t.
Proof.
elim: t => // [y T t1 IH | t1 IH1 t2 IH2 | t1 IH1 t2 IH2 t3 IH3] H.
- case: (y =P x) => [-> // | /eqP Hyx].
  apply: VS_Abs_2; auto.
    by apply: H; solve_mem.
  apply: IH.
  move=> z Hz.
  apply: H.
  by solve_mem.
- by apply: VS_App;
     [apply: IH1 | apply: IH2];
     move=> y Hy;
     apply: H;
     solve_mem.
- by apply: VS_If;
     [apply: IH1 | apply: IH2 | apply: IH3];
     move=> y Hy;
     apply: H;
     solve_mem.
Qed.

Lemma valid_subst_subst x s t a b :
  valid_subst x s t ->
  b \notin Vars (App (Var x) (App s t)) ->
  valid_subst x s ({a \-> Var b} t).
Proof.
elim: x s t / => //= [x s y | x s T t1 | x s y T t1 y_neq_x y_notin_FV VS IH | x s y T t1 y_neq_x x_notin_FV VS _ | x s t1 t2 VS1 IH1 VS2 IH2 | x s t1 t2 t3 VS1 IH1 VS2 IH2 VS3 IH3] b_notin_Vars.
- by case: (y =P a).
- by case: (x =P a).
- case: (y =P a) => *; apply: VS_Abs_2; solve_mem.
  by apply: IH; rewrite !in_Vars_App; solve_mem. (* the rewrite tactic is needed. *)
- case: (y =P a) => _; auto.
  apply: notin_FV_valid_subst_xst.
  rewrite in_FV_Abs; apply/nandP; right.
  by apply: notin_FV_notin_FV_subst; solve_mem.
- apply: VS_App.
    by apply: IH1; rewrite !in_Vars_App; solve_mem.
  by apply: IH2; rewrite !in_Vars_App; solve_mem.
- apply: VS_If.
  + by apply: IH1; rewrite !in_Vars_App; solve_mem.
  + by apply: IH2; rewrite !in_Vars_App; solve_mem.
  + by apply: IH3; rewrite !in_Vars_App; solve_mem.
Qed.
End Subst.
