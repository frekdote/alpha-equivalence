From mathcomp Require Import all_ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

From LC Require Import term tactics.

Goal forall x y, x \notin Vars (Var y) -> x != y.
Proof.
move=> *; solve_mem.
Qed.

Goal forall x y, x \notin Vars (Var y) -> y != x.
Proof.
move=> *; solve_mem.
Qed.

Goal forall x y t1 t2, x \notin Vars (App t1 (App (Var y) t2)) -> (x \notin Vars t2) && (y != x) && (x \notin Vars t1).
Proof.
move=> *; solve_mem.
Qed.  

Goal forall x y T t1, x \notin Vars (Abs y T t1) -> x \notin FV t1.
Proof.
move=> *; solve_mem.
Qed.

Goal forall x y T t1, x \notin Vars (Abs y T t1) -> x \notin BV t1.
Proof.
move=> *; solve_mem.
Qed.

Goal forall x t1 t2, x \notin Vars (App t1 t2) -> (x \notin Vars t1 -> x == 3) -> x == 3.
Proof.
move=> *; solve_mem.
Qed.

Goal forall x t1 t2, x \notin Vars (App t1 t2) -> (x \notin Vars (App t1 t2) -> x == 3) -> x == 3.
Proof.
move=> *.
solve_mem.
Qed.

Goal forall x y, y != x -> x \notin Vars (Var y).
Proof.
move=> *; solve_mem.
Qed.

Goal forall x y T t1, x \notin FV t1 -> x \notin FV (Abs y T t1).
Proof.
move=> *; solve_mem.
Qed.

Goal forall x y z T t1, x \in FV (Abs y T t1) -> z != x -> x \in FV (Abs z T t1).
Proof.
move=> *; solve_mem.
Qed.

Goal forall x t1 t1' t2, x \in FV (App t1 t2) -> (x \in FV t1 -> x \in FV t1') -> x \in FV (App t1' t2).
Proof.
move=> *; solve_mem.
Qed.

Goal forall x y T t1, x \notin FV (Abs y T t1) -> (x == y) || (x != y) && (x \notin FV t1).
Proof.
move=> ? ? ?.
solve_mem.
Qed.

Goal forall x T t1, x \notin FV t1 -> (x \notin FV t1 -> False -> True) -> x \notin FV (Abs x T t1).
Proof.
move=> ? ? ? ? ?.
solve_mem.
Qed.

Goal forall x y, x \notin Vars (Var y) -> y == x -> false.
Proof.
move=> *.
solve_mem.
Qed.
