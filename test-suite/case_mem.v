From mathcomp Require Import all_ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

From LC Require Import term tactics.

Goal forall x y, x \in Vars (Var y) -> x == y.
Proof.
move=> ? ?.
case_mem.
auto.
Qed.

Goal forall x y T t1, x \in Vars (Abs y T t1) -> (x == y) || (x \in Vars t1).
Proof.
move=> ? ? ? ?.
case_mem.
all: move=> *; apply/orP; auto.
Qed.

Goal forall x y, x \notin Vars (Var y) -> x != y.
Proof.
move=> ? ?.
case_mem.
auto.
Qed.

Goal forall x t1 t2, x \notin Vars (App t1 t2) -> (x \notin Vars t1) || (x \notin Vars t2).
Proof.
move=> ? ? ?.
case_mem.
all: move=> *; apply/orP; auto.
Qed.

Goal forall x, x \in Vars Tru -> 1 = 2.
Proof.
move=> ?.
case_mem.
Qed.

Goal forall x, x \notin Vars Fal -> True.
Proof.
move=> ?.
case_mem.
constructor.
Qed.

Goal forall x t1 t2 t3, x \in Vars (IfExp t1 t2 t3) -> (x \in Vars t1) || (x \in Vars t2) || (x \in Vars t3).
Proof.
move=> ? ? ? ?.
case_mem.
all: move=> ->; rewrite ?orbT //.
Qed.

Goal forall x t1 t2 t3, x \notin Vars (IfExp t1 t2 t3) -> (x \notin Vars t1).
Proof.
move=> ? ? ? ?.
case_mem.
auto.
Qed.

Goal forall x y, x \in FV (Var y) -> x == y.
Proof.
move=> ? ?.
case_mem.
auto.
Qed.

Goal forall x y T t1, x \in FV (Abs y T t1) -> (x != y).
Proof.
move=> ? ? ? ?.
case_mem.
auto.
Qed.

Goal forall x t1 t2, x \in FV (App t1 t2) -> (x \in FV t1) || (x \in FV t2).
Proof.
move=> ? ? ?.
case_mem.
all: move=> *; apply/orP; auto.
Qed.

Goal forall x y, x \notin FV (Var y) -> x != y.
Proof.
move=> ? ?.
case_mem.
auto.
Qed.

Goal forall x y T t1, x \notin FV (Abs y T t1) -> (x == y) || (x \notin FV t1).
Proof.
move=> ? ? ? ?.
case_mem.
all: move=> *; apply/orP; auto.
Qed.

Goal forall x t1 t2, x \notin FV (App t1 t2) -> x \notin FV t2.
Proof.
move=> ? ? ?.
case_mem.
auto.
Qed.

Goal forall x y, x \in BV (Var y) -> 1 = 2.
Proof.
move=> ? ?.
case_mem.
Qed.

Goal forall x y T t1, x \in BV (Abs y T t1) -> (x == y) || (x \in BV t1).
Proof.
move=> ? ? ? ?.
case_mem.
all: move=> *; apply/orP; auto.
Qed.

Goal forall x t1 t2, x \notin BV (App t1 t2) -> x \notin BV t1.
Proof.
move=> ? ? ?.
case_mem.
auto.
Qed.

Goal forall x y, x \in Vars (Var y) -> x == y.
Proof.
move=> ? ? H.
case_mem H.
auto.
Qed.
