From mathcomp Require Import all_ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

From LC Require Import term tactics.

Goal forall x, x \in Vars Fal -> 1 = 2.
Proof.
move=> ? H.
deep_case_mem H.
Qed.

Goal forall x, x \notin BV Tru -> True.
Proof.
move=> ? H.
deep_case_mem H.
auto.
Qed.

Goal forall x t1 t2 t3 t4, x \in Vars (IfExp t1 t2 (App t3 t4)) -> (x \in Vars t1) || (x \in Vars t2) || (x \in Vars t3) || (x \in Vars t4).
Proof.
move=> ? ? ? ? ? H.
deep_case_mem H.
all: move=> ->; rewrite ?orbT //.
Qed.

Goal forall x t1 y T t2 t3 t4, x \notin FV (IfExp t1 (Abs y T t2) (App t3 t4)) -> (x == y) || (x \notin FV t2).
Proof.
move=> ? ? ? ? ? ? ? H.
deep_case_mem H.
all: move=> *;
              repeat match goal with
                     | H : is_true ?b |- context [?b] => rewrite H /=
                     | _ => rewrite ?orbT //
                     end.
Qed.  

Goal forall x y t, x \in Vars (App (Var y) t) -> (x == y) || (x \in Vars t).
Proof.
move=> ? ? ? H.
deep_case_mem H.
all: move=> *; apply/orP; auto.
Qed.

Goal forall x y t, x \notin Vars (App (Var y) t) -> x != y.
Proof.
move=> ? ? ?.
deep_case_mem.
auto.
Qed.

Goal forall x y T z t1 t2, x \in Vars (App (Abs y T t1) (App t2 (Var z))) -> (x == y) || (x \in Vars t1) || (x \in Vars t2) || (x == z).
Proof.
move=> ? ? ? ? ? ?.
deep_case_mem.
all: move=> *;
              repeat match goal with
                     | H : is_true ?x |- context [?x] => rewrite H
                     | _ => rewrite ?(orbT, andbF) //=
                     end.
Qed.

Goal forall x y T t1 t2, x \notin Vars (App t1 (Abs y T t2)) -> (x != y) && (x \notin Vars t1) && (x \notin Vars t2).
Proof.
move=> ? ? ? ? ?.
deep_case_mem.
all: move=> *;
              repeat match goal with
                     | H : is_true ?x |- context [?x] => rewrite H
                     | _ => rewrite ?(orbT, andbF) //=
                     end.
Qed.

Goal forall x y T t1 t2, x \in FV (Abs y T (App t1 t2)) -> (x == y) || (x \in FV t1) || (x \in FV t2).
Proof.
move=> ? ? ? ? ?.
deep_case_mem.
all: move=> *;
              repeat match goal with
                     | H : is_true ?x |- context [?x] => rewrite H
                     | _ => rewrite ?(orbT, andbF) //=
                     end.
Qed.

Goal forall x y T z t, x \in BV (Abs y T (App t (Var z))) -> (x == y) || (x \in BV t).
Proof.
move=> ? ? ? ? ?.
deep_case_mem.
all: move=> *;
              repeat match goal with
                     | H : is_true ?x |- context [?x] => rewrite H
                     | _ => rewrite ?(orbT, andbF) //=
                     end.
Qed.

Goal forall x y T t1 t2, x \in Vars (Abs y T (App t1 t2)) -> (x == y) || (x \in Vars (App t1 t2)) && (x \in Vars t1) || (x \in Vars t2).
Proof.
move=> ? ? ? ? ?.
deep_case_mem.
all: move=> *;
              repeat match goal with
                     | H : is_true ?x |- context [?x] => rewrite H
                     | _ => rewrite ?(orbT, andbF) //=
                     end.
Qed.
